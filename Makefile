#PROGS	= klf200mqtt
PROGS	+= klf200cli
PROGS	+= klf200mqtt
default	: $(PROGS)

PREFIX	= /usr/local

CC	= gcc
CFLAGS	= -Wall
CPPFLAGS= -D_GNU_SOURCE
LDLIBS	= -lssl -lcrypto -lm -lrt
INSTOPTS= -s

VERSION := $(shell git describe --tags --always)

-include config.mk

CPPFLAGS += -DVERSION=\"$(VERSION)\"

klf200mqtt: LDLIBS+=-lmosquitto
klf200mqtt: \
	lib/libt.o lib/libe.o \
	klf200iface.o \
	klf200table.o \
	slip.o \
	lib/libnetrc.o \
	lib/libtable.o \
	lib/liburi.o

klf200cli: \
	klf200iface.o \
	klf200table.o \
	slip.o \
	lib/libnetrc.o \
	lib/libtable.o \
	lib/liburi.o

install: $(PROGS)
	$(foreach PROG, $(PROGS), install -vp -m 0777 $(INSTOPTS) $(PROG) $(DESTDIR)$(PREFIX)/bin/$(PROG);)

clean:
	rm -rf $(wildcard *.o lib/*.o) $(PROGS)
