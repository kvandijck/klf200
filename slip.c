#include <stdint.h>
#include <string.h>

#define SLIP_END	0xc0
#define SLIP_ESC	0xdb
#define SLIP_ESC_END	0xdc
#define SLIP_ESC_ESC	0xdd

int slip(const void *vsrc, int len, void *vdst, int dstlen)
{
	const uint8_t *src = vsrc;
	uint8_t *dst = vdst;
	int spos, dpos;

	dst[0] = SLIP_END;
	for (spos = 0, dpos = 1; spos < len; ++spos) {
		if (dpos + 2 >= dstlen)
			return -1;
		if (src[spos] == SLIP_END) {
			dst[dpos++] = SLIP_ESC;
			dst[dpos++] = SLIP_ESC_END;
		} else if (src[spos] == SLIP_ESC) {
			dst[dpos++] = SLIP_ESC;
			dst[dpos++] = SLIP_ESC_ESC;
		} else
			dst[dpos++] = src[spos];
	}
	if (dpos +1 >= dstlen)
		return -1;
	dst[dpos++] = SLIP_END;
	return dpos;
}

int slip_frame_len(const void *vsrc, int len)
{
	const uint8_t *src = vsrc, *end;

	end = memchr(src, SLIP_END, len);
	return end ? (end - src + 1): -1;
}

int unslip(const void *vsrc, int len, void *vdst, int dstlen)
{
	const uint8_t *src = vsrc;
	uint8_t *dst = vdst;
	int spos, dpos;

	for (spos = dpos = 0; spos < len; ++spos) {
		if (src[spos] == SLIP_END)
			break;

		/* test room */
		if (dpos + 1 >= dstlen)
			return -1;

		if (src[spos] == SLIP_ESC) {
			++spos;
			dst[dpos++] = (src[spos] == SLIP_ESC_ESC) ? SLIP_ESC : SLIP_END;
		} else
			dst[dpos++] = src[spos];
	}
	return dpos;
}
