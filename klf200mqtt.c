#include <errno.h>
#include <math.h>
#include <signal.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <unistd.h>
#include <fcntl.h>
#include <fnmatch.h>
#include <getopt.h>
#include <syslog.h>
#include <sys/signalfd.h>
#include <sys/uio.h>

#include <mosquitto.h>
#include "lib/libt.h"
#include "lib/libe.h"
#include "lib/libnetrc.h"
#include "lib/libtimechange.h"
#include "lib/libtable.h"
#include "klf200.h"

#define NAME "klf200mqtt"
#ifndef VERSION
#define VERSION "<undefined version>"
#endif

/* generic error logging */
#define LOG_MQTT	0x4000000

/* safeguard our LOG_EXIT extension */
#if (LOG_MQTT & (LOG_FACMASK | LOG_PRIMASK))
#error LOG_MQTT conflict
#endif
static void mqttlog(int loglevel, const char *str);

static int max_loglevel = LOG_WARNING;
__attribute__((format(printf,2,3)))
void mylog(int loglevel, const char *fmt, ...)
{
	static int logtostderr = -1;
	va_list va;
	char *str;

	if (logtostderr < 0) {
		int fd;

		fd = open("/dev/tty", O_RDONLY | O_NOCTTY);
		if (fd >= 0)
			close(fd);
		/* log to stderr when we're in a terminal */
		logtostderr = fd >= 0;
		if (!logtostderr) {
			openlog(NAME, 0, LOG_LOCAL2);
			setlogmask(LOG_UPTO(max_loglevel));
		}
	}

	/* render string */
	va_start(va, fmt);
	vasprintf(&str, fmt, va);
	va_end(va);

	if (!logtostderr) {
		syslog(loglevel & LOG_PRIMASK, "%s", str);

	} else if ((loglevel & LOG_PRIMASK) <= max_loglevel) {
		struct timespec tv;
		char timbuf[64];

		clock_gettime(CLOCK_REALTIME, &tv);
		strftime(timbuf, sizeof(timbuf), "%b %d %H:%M:%S", localtime(&tv.tv_sec));
		sprintf(timbuf+strlen(timbuf), ".%03u ", (int)(tv.tv_nsec/1000000));

		struct iovec vec[] = {
			{ .iov_base = timbuf, .iov_len = strlen(timbuf), },
			{ .iov_base = NAME, .iov_len = strlen(NAME), },
			{ .iov_base = ": ", .iov_len = 2, },
			{ .iov_base = str, .iov_len = strlen(str), },
			{ .iov_base = "\n", .iov_len = 1, },
		};
		writev(STDERR_FILENO, vec, sizeof(vec)/sizeof(vec[0]));
	}
	if (loglevel & LOG_MQTT) {
		mqttlog(loglevel, str);
	}

	free(str);
	if ((loglevel & LOG_PRIMASK) <= LOG_ERR)
		exit(1);
}
#define ESTR(num)	strerror(num)

/* program options */
static const char help_msg[] =
	NAME ": bridge between KLF200 IO-homecontrol and MQTT\n"
	"usage:	" NAME " [OPTIONS ...]\n"
	"\n"
	"Options\n"
	" -V, --version		Show version\n"
	" -v, --verbose		Be more verbose\n"
	" -h, --host=URI	Specify KLF200 hostname(:port)\n"
	" -m, --mqtt=HOST[:PORT]Specify alternate MQTT host+port\n"
	" -p, --prefix=PREFIX	Publish under PREFIX (default hc/)\n"
	"\n"
	"Special topics\n"
	" config/" NAME "/statusinterval	interval for status updates (default 60)\n"
	;

#ifdef _GNU_SOURCE
static struct option long_opts[] = {
	{ "help", no_argument, NULL, '?', },
	{ "version", no_argument, NULL, 'V', },
	{ "verbose", no_argument, NULL, 'v', },

	{ "host", required_argument, NULL, 'h', },
	{ "mqtt", required_argument, NULL, 'm', },
	{ "prefix", required_argument, NULL, 'p', },

	{ },
};
#else
#define getopt_long(argc, argv, optstring, longopts, longindex) \
	getopt((argc), (argv), (optstring))
#endif
static const char optstring[] = "Vv?h:m:p:";

/* signal handler */
static volatile int sigterm;

static void signalrecvd(int fd, void *dat)
{
	int ret;
	struct signalfd_siginfo sfdi;

	for (;;) {
		ret = read(fd, &sfdi, sizeof(sfdi));
		if (ret < 0 && errno == EAGAIN)
			break;
		if (ret < 0)
			mylog(LOG_ERR, "read signalfd: %s", ESTR(errno));
		switch (sfdi.ssi_signo) {
		case SIGTERM:
		case SIGINT:
			sigterm = 1;
			break;
		}
	}
}

/* self-sync util */
static int ready;
static char myuuid[128];
static const char selfsynctopic[] = "tmp/selfsync";
static void send_self_sync(struct mosquitto *mosq, int qos)
{
	int ret;

	sprintf(myuuid, "%i-%lli-%i", getpid(), (long long)time(NULL), rand());

	ret = mosquitto_subscribe(mosq, NULL, selfsynctopic, qos);
	if (ret)
		mylog(LOG_ERR, "mosquitto_subscribe %s: %s", selfsynctopic, mosquitto_strerror(ret));
	ret = mosquitto_publish(mosq, NULL, selfsynctopic, strlen(myuuid), myuuid, qos, 0);
	if (ret < 0)
		mylog(LOG_ERR, "mosquitto_publish %s: %s", selfsynctopic, mosquitto_strerror(ret));
}

static int is_self_sync(const struct mosquitto_message *msg)
{
	return !strcmp(msg->topic, selfsynctopic) &&
		!strcmp(myuuid, msg->payload ?: "");
}

/* usefull tricks */
__attribute__((format(printf,1,2)))
static const char *csprintf(const char *fmt, ...)
{
	va_list va;
	static char *str;

	if (str)
		free(str);
	str = NULL;
	va_start(va, fmt);
	vasprintf(&str, fmt, va);
	va_end(va);
	return str;
}

/* return string with trailing decimal 0's stripped */
__attribute__((unused))
static const char *mydtostr(double d)
{
	static char buf[64];
	char *str;
	int ptpresent = 0;

	sprintf(buf, "%lg", d);
	for (str = buf; *str; ++str) {
		if (*str == '.')
			ptpresent = 1;
		else if (*str == 'e')
			/* nothing to do anymore */
			break;
		else if (ptpresent && *str == '0') {
			int len = strspn(str, "0");
			if (!str[len]) {
				/* entire string (after .) is '0' */
				*str = 0;
				if (str > buf && *(str-1) == '.')
					/* remote '.' too */
					*(str-1) = 0;
				break;
			}
		}
	}
	return buf;
}

/* parameters */
static char *klfuri;
static int klfsock;

static const char *mqtt_host = "localhost";
static int mqtt_port = 1883;
static int mqtt_keepalive = 10;
static int mqtt_qos = 0;

static const char *mqtt_write_suffix = "/set";
static const char *mqtt_klf_suffix = "/klf200def";
static const char *mqtt_prefix = "hc/";
static int mqtt_prefix_len = 3;
static double house_monitor_interval = 15*60;

static char *addr_table_file;

static time_t last_log_time;
static int log_req_scheduled;

/* usefull cache */
static uint8_t tdat[256];
static struct mosquitto *mosq;

/* item */
struct item {
	struct item *next;
	struct item *prev;

	char *lastpayload;

	int cookie; /* various use */
	int nidx;
#define GRPMAX 20
	uint8_t nodes[GRPMAX];

	uint8_t origin;
	uint8_t priority;
	uint16_t lock, unlock;
	uint8_t locktime;

	char *topic;
	int topiclen;
	int state;
	int avgdir, lastdir;
	int cmdtype;
	int cmdaddr;

	const char *(*btnfunc)(const struct item *, const char *str);
};
static struct item *items;

struct node {
	double val;
	uint64_t serial;
	int dir;
	int flags;
#define NF_VALID	(1 << 0)
#define NF_BUSY		(1 << 1)
	/* attributes of last successfull command */
	uint8_t cmdtype;
	uint32_t cmdaddr;
};

static struct node nodes[256];

static void myklf200send(struct item *item, int sock, int cmd, const void *tdat, int tlen, double timeout);
static void myklf200sendf(struct item *item, int sock, int cmd, int resp_cmd, const void *dat, int len, double timeout,
		int final_cmd, double final_timeout);
static void myklf200_ping(void *dat);
static void myklf200_static_monitor(void *);
static void mqtt_pub(const char *topic, const char *payload, int retain);

/* code */
static void delete_item(struct item *it)
{
	if (it->prev)
		it->prev->next = it->next;
	if (it->next)
		it->next->prev = it->prev;
	free(it->topic);
	if (it->lastpayload)
		free(it->lastpayload);
	free(it);
}

static void reset_item(struct item *it)
{
	int j;

	it->nidx = 0;
	/* initialize to middle position, to not disturb the avg */
	for (j = 0; j < GRPMAX; ++j)
		nodes[it->nodes[j]].val = 0.5;
	it->origin = 1; /* user */
	it->priority = 3; /* user2 */
	it->lock = it->unlock = 0xffff;
	it->locktime = 255;
	if (it->lastpayload) {
		free(it->lastpayload);
		it->lastpayload = NULL;
	}
	it->lastdir = it->avgdir = 0;
}

static struct item *new_item(void)
{
	struct item *it;

	it = malloc(sizeof(*it));
	if (!it)
		mylog(LOG_ERR, "malloc item: %s", ESTR(errno));
	memset(it, 0, sizeof(*it));
	reset_item(it);

	/* insert in linked list */
	it->next = items;
	if (it->next) {
		it->prev = it->next->prev;
		it->next->prev = it;
	} else
		it->prev = (struct item *)(((char *)&items) - offsetof(struct item, next));
	it->prev->next = it;
	return it;
}

static struct item *topictoitem(const char *topic, const char *suffix, int create)
{
	struct item *it;
	int len;

	len = strlen(topic ?: "") - strlen(suffix ?: "");
	if (len < 0)
		return NULL;
	/* match suffix */
	if (strcmp(topic+len, suffix ?: ""))
		return NULL;
	/* match base topic */
	for (it = items; it; it = it->next) {
		if ((it->topiclen == len) && !strncmp(it->topic ?: "", topic, len))
			return it;
	}
	if (!create)
		return NULL;

	it = new_item();
	it->topic = strdup(topic);
	it->topic[len] = 0;
	it->topiclen = len;
	return it;
}

/* MQTT iface */
__attribute__((format(printf,1,2)))
static const char *topicfmt(const char *fmt, ...)
{
	static char *result;
	va_list va;

	if (result)
		free(result);
	va_start(va, fmt);
	vasprintf(&result, fmt, va);
	va_end(va);
	return result;
}
__attribute__((format(printf,1,2)))
static const char *payloadfmt(const char *fmt, ...)
{
	static char *result;
	va_list va;

	if (result)
		free(result);
	va_start(va, fmt);
	vasprintf(&result, fmt, va);
	va_end(va);
	return result;
}

static double item_avgval(const struct item *it)
{
	int j;
	double v;

	for (v = 0, j = 0; j < it->nidx; ++j)
		v += nodes[it->nodes[j]].val;
	return v/it->nidx;
}

static int item_avgdir(const struct item *it)
{
	int j;
	int v;
	double dv;

	for (v = 0, j = 0; j < it->nidx; ++j)
		v += nodes[it->nodes[j]].dir;

	dv = v*1.0/it->nidx;
	if (dv < -0.01)
		return -1;
	else if (dv > 0.01)
		return 1;
	else
		return 0;
}

static int item_cmdtype(const struct item *it)
{
	int j;
	int result;

	if (it->nidx < 1)
		return -1;
	result = nodes[it->nodes[0]].cmdtype;
	for (j = 1; j < it->nidx; ++j) {
		if (result != nodes[it->nodes[j]].cmdtype)
			return -1;
	}
	return result;
}

static int item_cmdaddr(const struct item *it)
{
	int j;
	int result;

	if (it->nidx < 1)
		return 0;
	result = nodes[it->nodes[0]].cmdaddr;
	for (j = 1; j < it->nidx; ++j) {
		if (result != nodes[it->nodes[j]].cmdaddr)
			return 0;
	}
	return result;
}

static const char *btn_move_stop(const struct item *it, const char *str)
{
	/* move/stpo value */
	if (!strcmp(str, "1"))
		return (it->lastdir > 0) ? "0" : "1";
	else
		return "stop";
}

static const char *btn_simple(const struct item *it, const char *str)
{
	/* static value, so ignore input, act like button-press */
	return "btn";
}

static void send_command(struct item *it, const char *payload)
{
	int session, j, idx;
	uint16_t raw16;

	if (!payload)
		payload = "";
	if (!strcmp(payload, "wink")) {
		memset(tdat, 0, sizeof(tdat));
		/* initialize defaults */
		session = klf200_sessionid();
		klf200_put16(tdat, session);
		tdat[2] = it->origin;
		tdat[3] = it->priority;
		tdat[4] = 1; /* enable wink */
		tdat[5] = 5; /* wink time */
		tdat[6] = it->nidx;
		for (j = 0; j < it->nidx; ++j)
			tdat[7+j] = it->nodes[j];

		myklf200sendf(it, klfsock, GW_WINK_SEND_REQ, GW_WINK_SEND_CFM,
				tdat, 27, 3, GW_WINK_SEND_NTF, 5);
		mylog(LOG_INFO, "%s wink: session %u", it->topic, session);
		return;
	}
	if (!strcmp(payload, "config")) {
		memset(tdat, 0, 26);
		for (j = 0; j < it->nidx; ++j) {
			idx = it->nodes[j];
			tdat[idx/8] |= 1 << (idx % 8);
		}
		myklf200send(it, klfsock, GW_CS_ACTIVATE_CONFIGURATION_MODE_REQ, tdat, 26, 3);
		mylog(LOG_INFO, "%s set for config", it->topic);
		return;
	}

	if (!strcmp(payload, "btn")) {
		double value;

		value = item_avgval(it);

		if (it->avgdir)
			payload = "stop";
		else if (value < 0.01)
			payload = "1";
		else if (value > 0.99)
			payload = "0";
		else if (it->lastdir > 0)
			payload = "0";
		else
			payload = "1";
		mylog(LOG_NOTICE, "%s btn -> %s (%i %i)", it->topic, payload, it->avgdir, it->lastdir);
	}

	memset(tdat, 0, sizeof(tdat));
	/* initialize defaults */
	session = klf200_sessionid();
	klf200_put16(tdat, session);
	tdat[2] = it->origin;
	tdat[3] = it->priority;
	/* main value: default is nan */
	raw16 = klf200_encode_str(payload, NULL);
	klf200_put16(tdat+7, raw16);

	/* lock if sending a real value */
	if (it->lock != 0xffff) {
		int lock;

		lock = raw16 != 0xd400;
		/* put lock data */
		tdat[62] = 1;
		klf200_put16(tdat+63, lock ? it->lock : it->unlock);
		tdat[65] = it->locktime;
	}

	tdat[41] = it->nidx;
	for (j = 0; j < it->nidx; ++j)
		tdat[42+j] = it->nodes[j];
	/* perform action */
	myklf200sendf(it, klfsock, GW_COMMAND_SEND_REQ, GW_COMMAND_SEND_CFM,
			tdat, 66, 3, GW_SESSION_FINISHED_NTF, 120);
	mylog(LOG_INFO, "%s set: session %u", it->topic, session);
}

static void mqtt_pub(const char *topic, const char *payload, int retain)
{
	int ret;

	ret = mosquitto_publish(mosq, NULL, topic, strlen(payload?:""), payload, mqtt_qos, retain);
	if (ret < 0)
		mylog(LOG_ERR, "mosquitto_publish %s: %s", topic, mosquitto_strerror(ret));
}

static void mqttlog(int loglevel, const char *msg)
{
	static const char *const prionames[] = {
		[LOG_EMERG] = "emerg",
		[LOG_ALERT] = "alert",
		[LOG_CRIT] = "crit",
		[LOG_ERR] = "err",
		[LOG_WARNING] = "warn",
		[LOG_NOTICE] = "notice",
		[LOG_INFO] = "info",
		[LOG_DEBUG] = "debug",
	};

	mqtt_pub(topicfmt("log/%s/%s", NAME, prionames[loglevel & LOG_PRIMASK]), msg, 0);
}

static void send_item_mqtt(struct item *it, int initial)
{
	const char *payload;

	int cmdaddr = item_cmdaddr(it);
	int cmdtype = item_cmdtype(it);

	if (initial || cmdaddr != it->cmdaddr) {
		const char *name;
		const char *addrstr;

		addrstr = cmdaddr ? payloadfmt("%06x", cmdaddr) : NULL;
		mqtt_pub(topicfmt("%s/last/addr", it->topic), addrstr, 1);

		name = cmdaddr ? libtable_lookup(addrstr, addr_table_file, LTL_FNMATCH) : NULL;
		mqtt_pub(topicfmt("%s/last/name", it->topic), name, 1);
	}

	if (initial || cmdtype != it->cmdtype || cmdaddr != it->cmdaddr)
		/* repeat type if only addr is different */
		mqtt_pub(topicfmt("%s/last/type", it->topic), (cmdtype != -1) ? klf200_string(klf200_owner_str, cmdtype) : "", 1);

	it->cmdaddr = cmdaddr;
	it->cmdtype = cmdtype;

	payload = klf200_dtostr(item_avgval(it), NULL);
	if (initial || strcmp(it->lastpayload ?: "", payload)) {
		if (it->lastpayload)
			free(it->lastpayload);
		it->lastpayload = strdup(payload);
		mqtt_pub(it->topic, it->lastpayload, 1);
	}
	/* handle dir */
	int avgdir = item_avgdir(it);
	if (initial || avgdir != it->avgdir) {
		it->avgdir = avgdir;
		mqtt_pub(topicfmt("%s/dir", it->topic), payloadfmt("%i", avgdir), 1);
		if (it->btnfunc == btn_move_stop)
			/* publish btn state */
			mqtt_pub(topicfmt("%s/btn", it->topic), avgdir ? "1" : "0", 1);
	}
	if (avgdir)
		it->lastdir = avgdir;
}

static void node_changed(int idx)
{
	int j;
	struct item *it;

	for (it = items; it; it = it->next) {
		for (j = 0; j < it->nidx; ++j) {
			if (it->nodes[j] == idx) {
				send_item_mqtt(it, 0);
				break;
			}
		}
	}
}

static void wipe_items_mqtt(void)
{
	struct item *it;

	for (it = items; it; it = it->next) {
		mqtt_pub(it->topic, "", 1);
		mqtt_pub(topicfmt("%s/dir", it->topic), "", 1);
		mqtt_pub(topicfmt("%s/dir/btn", it->topic), "", 1);
		mqtt_pub(topicfmt("%s/last/addr", it->topic), "", 1);
		mqtt_pub(topicfmt("%s/last/name", it->topic), "", 1);
		mqtt_pub(topicfmt("%s/last/type", it->topic), "", 1);
	}
}

static void my_mqtt_log(struct mosquitto *mosq, void *userdata, int level, const char *str)
{
	static const int logpri_map[] = {
		MOSQ_LOG_ERR, LOG_ERR,
		MOSQ_LOG_WARNING, LOG_WARNING,
		MOSQ_LOG_NOTICE, LOG_NOTICE,
		MOSQ_LOG_INFO, LOG_INFO,
		MOSQ_LOG_DEBUG, LOG_DEBUG,
		0,
	};
	int j;

	for (j = 0; logpri_map[j]; j += 2) {
		if (level & logpri_map[j]) {
			mylog(logpri_map[j+1], "[mosquitto] %s", str);
			return;
		}
	}
}

static int test_suffix(const char *topic, const char *suffix)
{
	int len;

	len = strlen(topic ?: "") - strlen(suffix ?: "");
	if (len < 0)
		return 0;
	/* match suffix */
	return !strcmp(topic+len, suffix ?: "");
}

static void my_mqtt_msg(struct mosquitto *mosq, void *dat, const struct mosquitto_message *msg)
{
	struct item *it;
	int forme, ival, j;
	char *tok, *val, *hostname, *payload;

	if (is_self_sync(msg)) {
		ready = 1;
		return;
	}

	mylog(LOG_DEBUG, "mqtt:<%s %s", msg->topic, (char *)msg->payload ?: "<null>");
	if (test_suffix(msg->topic, mqtt_klf_suffix)) {
		/* this is an IOHC config parameter */
		payload = (char *)msg->payload ?: "";
		if (!strncmp(msg->payload ?: "", "host=", 5)) {
			hostname = strtok(payload, " \t")+5;
			forme = !strcmp(klfuri, hostname);
			payload = NULL;
		} else {
			hostname = "*";
			forme = 1;
		}

		it = topictoitem(msg->topic, mqtt_klf_suffix, msg->payloadlen && forme);
		if (!it) {
			mylog(LOG_INFO, "klf200 spec %s", msg->payloadlen ? hostname : "not for me");
			return;
		}

		/* this is a spec msg */
		if (!msg->payloadlen || !forme) {
			if (it) {
				mylog(LOG_INFO | LOG_MQTT, "removed klf200 spec for %s", it->topic);
				delete_item(it);
			}
			return;
		}

		/* clear first */
		if (it->lastpayload)
			mqtt_pub(it->topic, NULL, 1);
		reset_item(it);

		/* parse config */
		for (tok = strtok(payload, " \t"); tok; tok = strtok(NULL, " \t")) {
			val = strchr(tok, '=');
			if (val)
				*val++ = 0;
			if (!strcmp(tok, "idx")) {
				uint8_t idxs[GRPMAX];

				it->nidx = klf200_nodestrtolist(val, idxs, GRPMAX);
				for (j = 0; j < it->nidx; ++j)
					it->nodes[j] = idxs[j];

			} else if (!strcmp(tok, "btn")) {
				if (!strcmp(val, "movestop"))
					it->btnfunc = btn_move_stop;
				else if (!strcmp(val, "simple"))
					it->btnfunc = btn_simple;
				else
					mylog(LOG_WARNING | LOG_MQTT, "%s: btn function '%s' unkown", it->topic, val);

			} else if (!strcmp(tok, "origin")) {
				ival = klf200_string_idx(val, klf200_owner_str, 256);
				if (ival < 0)
					mylog(LOG_WARNING | LOG_MQTT, "%s: origin '%s' unknown", it->topic, val);
				it->origin = ival;

			} else if (!strcmp(tok, "priority")) {
				ival = klf200_string_idx(val, klf200_prio_level_str, 256);
				if (ival < 0)
					mylog(LOG_WARNING | LOG_MQTT, "%s: priority '%s' unknown", it->topic, val);
				it->priority = ival;

			} else if (!strcmp(tok, "lock")) {
				ival = klf200_string_idx(val, klf200_prio_level_str, 256);
				if (ival < 0)
					mylog(LOG_WARNING | LOG_MQTT, "%s: lock priority '%s' unknown", it->topic, val);
				it->lock &= ~(3 << (14-ival*2));
				it->lock |= 0 << (14-ival*2);
				it->unlock &= ~(3 << (14-ival*2));
				it->unlock |= 1 << (14-ival*2);

			} else if (!strcmp(tok, "locktime")) {
				if (!strcmp(val, "forever"))
					ival = 255;
				else
					ival = (strtoul(val, NULL, 0) + 29)/30;
				it->locktime = ival;
			}
		}
		mylog(LOG_INFO | LOG_MQTT, "new klf200 spec for %s", it->topic);
		/* try to recover info from other items, and publish if we found something */
		send_item_mqtt(it, 1);

	} else if (test_suffix(msg->topic, "/btn/set")) {
		it = topictoitem(msg->topic, "/btn/set", 0);
		if (!it)
			return;
		if (!it->btnfunc) {
			mylog(LOG_NOTICE | LOG_MQTT, "%s: using 'simple' btn", it->topic);
			it->btnfunc = btn_simple;
		}
		send_command(it, it->btnfunc(it, (const char *)msg->payload ?: ""));

	} else if (test_suffix(msg->topic, "/updown/set")) {
		it = topictoitem(msg->topic, "/updown/set", 0);
		if (!it)
			return;
		const char *payload = (const char *)msg->payload;
		if (it->avgdir)
			payload = "stop";
		send_command(it, payload);

	} else if (test_suffix(msg->topic, mqtt_write_suffix)) {
		if (msg->retain)
			/* ignore retained request */
			return;
		it = topictoitem(msg->topic, mqtt_write_suffix, 0);
		if (!it)
			return;
		send_command(it, (const char *)msg->payload);

#define CONFIG_NAME "config/" NAME "/"
	} else if (!strncmp(msg->topic, CONFIG_NAME, strlen(CONFIG_NAME))) {
		if (!strcmp(msg->topic + strlen(CONFIG_NAME), "statusinterval")) {
			house_monitor_interval = strtod((const char *)msg->payload, NULL);
			mylog(LOG_NOTICE, "house-monitor %.1lfs", house_monitor_interval);
			myklf200_static_monitor(NULL);
		}
	}
}

static void mqtt_maintenance(void *dat)
{
	int ret;
	struct mosquitto *mosq = dat;

	ret = mosquitto_loop_misc(mosq);
	if (ret)
		mylog(LOG_ERR, "mosquitto_loop_misc: %s", mosquitto_strerror(ret));
	libt_add_timeout(2.3, mqtt_maintenance, dat);
}

static void recvd_mosq(int fd, void *dat)
{
	struct mosquitto *mosq = dat;
	int evs = libe_fd_evs(fd);
	int ret;

	if (evs & LIBE_RD) {
		/* mqtt read ... */
		ret = mosquitto_loop_read(mosq, 1);
		if (ret)
			mylog(LOG_ERR, "mosquitto_loop_read: %s", mosquitto_strerror(ret));
	}
	if (evs & LIBE_WR) {
		/* flush mqtt write queue _after_ the timers have run */
		ret = mosquitto_loop_write(mosq, 1);
		if (ret)
			mylog(LOG_ERR, "mosquitto_loop_write: %s", mosquitto_strerror(ret));
	}
}

void mosq_update_flags(void)
{
	if (mosq)
		libe_mod_fd(mosquitto_socket(mosq), LIBE_RD | (mosquitto_want_write(mosq) ? LIBE_WR : 0));
}

/* klf200 interface */
struct request {
	struct request *next;
	struct request *prev;
	/* abstract item pointer, to find back replays per item */
	struct item *item;
	int sock;
	int nsent;
	double timeout;
	int session;
	int cmd;
	int resp_cmd;
	double final_timeout;
	int final_cmd;
	int len;
	uint8_t dat[2];
};

#define reqlog(LEVEL, req, fmt, ...) mylog((LEVEL), "%s%ssession %u,%03x: " fmt, \
		(req)->item ? (req)->item->topic : "", \
		(req)->item ? ": " : "", \
		(req)->session, (req)->cmd, \
		##__VA_ARGS__);

static void request_del(struct request *req)
{
	if (req->next)
		req->next->prev = req->prev;
	if (req->prev)
		req->prev->next = req->next;
	req->next = req->prev = NULL;
}

static inline struct request *root_fake_request(struct request **root)
{
	return (struct request *)(((char *)root) - offsetof(struct request, next));
}

static void request_add(struct request *req, struct request **root)
{
	request_del(req);
	req->next = *root;
	if (req->next) {
		req->prev = req->next->prev;
		req->next->prev = req;
	} else
		/* this is dirty: fake a struct request, which will only be used
		 * for setting the @next member
		 */
		req->prev = root_fake_request(root);
	req->prev->next = req;
}

#define MAX_REQUESTS	64
static struct request *requestq, *requestql;
static int nrequestq;
/* pending request */
static struct request *requestp;
/* running requests */
static struct request *requestr;
/* completed requests */
static struct request *requestc;

static void send_next_queued_request(void);
static void send_request(struct request *req);

static int get_session_id(int cmd, const void *dat, int len)
{
	switch (cmd) {
	case GW_COMMAND_SEND_REQ:
	case GW_COMMAND_SEND_CFM:
	case GW_STATUS_REQUEST_REQ:
	case GW_STATUS_REQUEST_CFM:
	case GW_STATUS_REQUEST_NTF:
	case GW_WINK_SEND_REQ:
	case GW_WINK_SEND_CFM:
		if (len >= 2)
			return klf200_get16(dat);
		break;
	}
	return 0;
}

static void session_forget(void *dat)
{
	struct request *req = dat;

	request_del(req);
	free(req);
}

static void cb_session_replay(void *dat)
{
	struct request *req = dat;

	send_request(req);
}

static void replay_session(int session, int node)
{
	struct request *req;

	for (req = requestc; req; req = req->next) {
		if (req->session == session)
			break;
	}
	if (!req)
		return;
	if (req->nsent >= 5) {
		reqlog(LOG_NOTICE | LOG_MQTT, req, "no more replay for node %i", node);
		return;
	}
	libt_remove_timeout(session_forget, req);
	libt_add_timeout(req->nsent*30, cb_session_replay, req);
	reqlog(LOG_INFO | LOG_MQTT, req, "replay for node %i ...", node);
}

static void remove_replays(struct item *item, int cmd)
{
	struct request *req;

	for (req = requestc; req; req = req->next) {
		if (req->item == item && req->cmd == cmd) {
			libt_remove_timeout(cb_session_replay, req);
			libt_remove_timeout(session_forget, req);
			reqlog(LOG_INFO | LOG_MQTT, req, "replay removed");
			request_del(req);
			free(req);
			break;
		}
	}
}

static void session_starved(void *dat)
{
	struct request *req = dat;

	reqlog(LOG_WARNING, req, "starved");
	request_del(req);
	free(req);
}

static void session_complete(int session, int cmd)
{
	struct request *req;

	for (req = requestr; req; req = req->next) {
		if (req->session == session && req->final_cmd == cmd)
			break;
	}
	if (!req)
		return;
	/* found running session */
	reqlog(LOG_INFO, req, "complete");
	libt_remove_timeout(session_starved, req);

	if (req->cmd == GW_COMMAND_SEND_REQ) {
		/* keep this session in the completed queue
		 * in case we receive a log 'could not contact ...'
		 * soon */
		request_add(req, &requestc);
		libt_add_timeout(5, session_forget, req);
	} else {
		request_del(req);
		free(req);
	}
}

static void session_lost(void *dat)
{
	struct request *req = dat;

	reqlog(LOG_WARNING, req, "lost");
	request_del(req);
	free(req);
	if (req == requestp)
		/* this was the top session, queue next */
		send_next_queued_request();
}
static void session_confirmed(int cmd, const void *dat, int len)
{
	int session;
	struct request *req;

	session = get_session_id(cmd, dat, len);
	req = requestp;
	if (!req || req->session != session || req->resp_cmd != cmd)
		return;

	libt_remove_timeout(session_lost, req);
	if (req->final_cmd) {
		/* move request to running list */
		request_add(req, &requestr);
		libt_add_timeout(req->final_timeout, session_starved, req);
	} else {
		request_del(req);
		free(req);
	}
	send_next_queued_request();
}

static void send_next_queued_request(void)
{
	struct request *req = requestql;

	if (!req) {
		requestp = requestql = NULL;
		return;
	}
	reqlog(LOG_INFO, req, "send");
	if (klf200_send(req->sock, 0, req->cmd, req->dat, req->len) < 0)
		mylog(LOG_ERR, "klf200 send %03x+%u failed", req->cmd, req->len);
	++req->nsent;
	libt_add_timeout(req->timeout, session_lost, req);
	/* postpone timeout */
	libt_add_timeout(300, myklf200_ping, NULL);

	if (req == requestql) {
		requestql = req->prev;
		if (requestql == root_fake_request(&requestq))
			requestql = NULL;
	}
	request_del(req);
	--nrequestq;
	requestp = req;
}

static void myklf200sendf(struct item *item, int sock, int cmd, int resp_cmd, const void *dat, int len, double timeout,
		int final_cmd, double final_timeout)
{
	int session = get_session_id(cmd, dat, len);
	struct request *req;

	if (nrequestq > MAX_REQUESTS) {
		mylog(LOG_WARNING | LOG_MQTT, "more than %u sessions queued, dropping %u,%03x", MAX_REQUESTS, session, cmd);
		return;
	}

	remove_replays(item, cmd);
	req = malloc(sizeof(*req)+len);
	memset(req, 0, sizeof(*req));
	req->item = item;
	req->sock = sock;
	req->timeout = timeout;
	req->session = session;
	req->cmd = cmd;
	req->resp_cmd = resp_cmd;
	req->final_cmd = final_cmd;
	req->final_timeout = final_timeout;
	req->len= len;
	memcpy(req->dat, dat, len);
	send_request(req);
}

static void send_request(struct request *req)
{
	if (!requestql)
		requestql = req;
	request_add(req, &requestq);
	++nrequestq;
	if (!requestp)
		send_next_queued_request();
	else
		reqlog(LOG_INFO, req, "queued");
}

static void myklf200send(struct item *item, int sock, int cmd, const void *dat, int len, double timeout)
{
	myklf200sendf(item, sock, cmd, cmd + 1, dat, len, timeout, 0, NAN);
}

static void myklf200send2(struct item *item, int sock, int cmd, int resp_cmd, const void *dat, int len, double timeout)
{
	myklf200sendf(item, sock, cmd, resp_cmd, dat, len, timeout, 0, NAN);
}

static void request_log(void *dat)
{
	log_req_scheduled = 0;
	klf200_put32(tdat, last_log_time -1);
	myklf200send2(NULL, klfsock, GW_GET_MULTIPLE_ACTIVATION_LOG_LINES_REQ,
			GW_GET_MULTIPLE_ACTIVATION_LOG_LINES_CFM, tdat, 4, 10);
}

static void myklf200_ping(void *dat)
{
	myklf200send(NULL, klfsock, GW_GET_STATE_REQ, NULL, 0, 0.5);
	libt_add_timeout(300, myklf200_ping, dat);
}

static int myklf200_house_monitor(int mask, int flags)
{
	int j, n, session, done;

	/* fill cmds */
	for (j = 0, done = 0; j < 256;) {
		memset(tdat, 0, sizeof(tdat));

		/* get status for next 20 nodes */
		for (n = 0; n < 20 && j < 256; ++j) {
			if (!(nodes[j].flags & NF_VALID))
				continue;
			if ((nodes[j].flags & mask) != flags)
				/* don't include this node */
				continue;
			tdat[3+n++] = j;
			++done;
		}
		if (!n)
			goto all_done;
		/* issue command */
		session = klf200_sessionid();
		klf200_put16(tdat, session);
		tdat[2] = n;
		tdat[23] = 3;
		/* perform action */
		myklf200sendf(NULL, klfsock, GW_STATUS_REQUEST_REQ, GW_STATUS_REQUEST_CFM,
				tdat, 26, 2, GW_SESSION_FINISHED_NTF, n*5);
	}
all_done:
	mylog(LOG_INFO, "%s %02x/%02x, %i nodes", __func__, flags, mask, done);
	return done;
}

static void myklf200_static_monitor(void *dat)
{
	myklf200_house_monitor(NF_BUSY, 0);
	libt_add_timeout(libt_timetointerval4(libt_walltime(), house_monitor_interval, -15, 15),
			myklf200_static_monitor, dat);
}

static uint8_t klfstate[6];
static int klfstate_recvd;

static void klf_pkt_recvd(void)
{
	int cmd = klf200_pktcmd();
	const uint8_t *dat = klf200_pktdat();
	int len = klf200_pktlen();
	int idx, dir;
	int session;
	char *msg = NULL;

	switch (cmd) {
	case GW_ERROR_NTF:
		if (len != 1)
			goto invalid;
		mylog(LOG_WARNING, "klf200 recvd error: %s", klf200_string(klf200_error_str, dat[0]));
		break;
	case GW_PASSWORD_ENTER_CFM:
		/* generic boolean response */
		if (len != 1)
			goto invalid;
		mylog(LOG_NOTICE | (dat[0] ? LOG_MQTT : 0), "klf200 password: %s", dat[0] ? "nok" : "ok");
		if (dat[0])
			exit(1);
		break;
	case GW_HOUSE_STATUS_MONITOR_DISABLE_CFM:
		msg = "not ";
	case GW_HOUSE_STATUS_MONITOR_ENABLE_CFM:
		if (len != 0)
			goto invalid;
		mylog(LOG_NOTICE, "%smonitoring ok", msg ?: "");
		break;
	case GW_GET_ALL_NODES_INFORMATION_FINISHED_NTF:
		mylog(LOG_NOTICE, "all nodes recvd");
		libt_add_timeout(2, myklf200_static_monitor, NULL);
		break;
	case GW_GET_ALL_GROUPS_INFORMATION_FINISHED_NTF:
		mylog(LOG_NOTICE, "all groups recvd");
		break;
	case GW_SET_UTC_CFM:
		mylog(LOG_NOTICE, "time synced");
		break;
	case GW_GET_ALL_NODES_INFORMATION_CFM:
	case GW_GET_ALL_GROUPS_INFORMATION_CFM:
		if (len != 2)
			goto invalid;
		mylog(LOG_NOTICE, "all %s: %u, %u elements",
				(cmd == GW_GET_ALL_NODES_INFORMATION_CFM) ? "nodes" : "groups",
				dat[0], dat[1]);
		break;
	case GW_GET_STATE_CFM:
		if (len != 6)
			goto invalid;
		if (!klfstate_recvd || memcmp(klfstate, dat, sizeof(klfstate)))
			mylog(LOG_NOTICE, "state %s: %s",
					klf200_string(klf200_state_str, dat[0]),
					klf200_string(klf200_substate_str, dat[1]));
		klfstate_recvd = 1;
		memcpy(klfstate, dat, sizeof(klfstate));
		break;

	case GW_GET_NODE_INFORMATION_NTF:
	case GW_GET_ALL_NODES_INFORMATION_NTF:
		if (len != 124)
			goto invalid;
		int baseseen = 0;
		idx = dat[0];

		nodes[idx].flags |= NF_VALID;
		nodes[idx].val = NAN;
		nodes[idx].serial = klf200_get64(dat+76);

		struct item *it;

		for (it = items; it; it = it->next) {
			/* test for single-node items */
			if (it->nidx != 1 || it->nodes[0] != idx)
				continue;
			if (!strncmp(it->topic, mqtt_prefix, mqtt_prefix_len)) {
				baseseen = 1;
				break;
			}
		}

		if (!baseseen) {
			it = new_item();

			it->nidx = 1;
			it->nodes[0] = dat[0];
			asprintf(&it->topic, "%s%u", mqtt_prefix, idx);
			it->topiclen = strlen(it->topic);
			mylog(LOG_NOTICE, "new item %s", it->topic);
		}
		break;
	case GW_NODE_STATE_POSITION_CHANGED_NTF:
		if (len != 20)
			goto invalid;

		uint16_t val16, set16;
		idx = dat[0];
		val16 = klf200_get16(dat+2);
		set16 = klf200_get16(dat+4);

#if 0
		mylog(LOG_NOTICE, "node %i changed: state %s, pos %s / %s",
				idx,
				klf200_string(klf200_nodestate_str, dat[1]),
				klf200_decode_str(val16),
				klf200_decode_str(set16));
#endif
		if (klf200_rawvalue_is_abs(val16))
			nodes[idx].val = klf200_decode_abs(val16);
		if (dat[1] != 4)
			/* not executing, reset dir */
			nodes[idx].dir = 0;

		else if (klf200_rawvalue_is_abs(set16)) {
			if (set16 < val16)
				nodes[idx].dir = -1;
			else if (set16 > val16)
				nodes[idx].dir = +1;
		}
		node_changed(idx);
		break;
	case GW_STATUS_REQUEST_NTF:
		if (len == 18 && dat[6] == 3) {
			uint16_t raw16, setp16;

			idx = dat[3];
			nodes[idx].cmdaddr = klf200_get24(dat+13);
			nodes[idx].cmdtype = dat[17];

			raw16 = klf200_get16(dat+9);
			if (klf200_rawvalue_is_abs(raw16))
				nodes[idx].val = klf200_decode_abs(raw16);
			setp16 = klf200_get16(dat+7);

			dir = 0;
			if (klf200_rawvalue_is_abs(setp16) && klf200_rawvalue_is_abs(raw16)) {
				if (setp16 < raw16)
					dir = -1;
				else if (setp16 > raw16)
					dir = 1;
			}

			if ((dat[4] == 2) && !(nodes[idx].flags & NF_BUSY)) {
				/* node active */
				nodes[idx].flags |= NF_BUSY;
				mylog(LOG_NOTICE, "node %i going %s", idx, (dir > 0) ? "down" : "up");
				nodes[idx].dir = dir;

			} else if ((dat[4] != 2) && (nodes[idx].flags & NF_BUSY)) {
				nodes[idx].flags &= ~NF_BUSY;
				mylog(LOG_NOTICE, "node %i %s", idx, "idle");
			}

			/* distribute */
			node_changed(idx);
		} else if (len == 59) {
		} else
			goto invalid;
		break;
	case GW_COMMAND_RUN_STATUS_NTF:
		if (len != 13)
			goto invalid;
#if 0
		mylog(LOG_NOTICE, "session %u: %s set node %u.%u to %s: %s, %s, info %08x",
				klf200_get16(dat),
				klf200_string(klf200_owner_str, dat[2]),
				dat[3], dat[4],
				klf200_decode_str(klf200_get16(dat+5)),
				klf200_string(klf200_runstatus_str, dat[7]),
				klf200_string(klf200_statusreply_str, dat[8]),
				klf200_get32(dat+9));
#endif
		idx = dat[3];
		/* pending */
		uint16_t rawvalue = klf200_get16(dat+5);
		if (klf200_rawvalue_is_abs(rawvalue))
			nodes[idx].val = klf200_decode_abs(rawvalue);

		if ((dat[7] == 2) && !(nodes[idx].flags & NF_BUSY)) {
			nodes[idx].flags |= NF_BUSY;
			mylog(LOG_NOTICE, "node %i %s", idx, "pending");

		} else if ((dat[7] != 2) && (nodes[idx].flags & NF_BUSY)) {
			nodes[idx].flags &= ~NF_BUSY;
			mylog(LOG_NOTICE, "node %i %s", idx,
					klf200_string(klf200_runstatus_str, dat[7]));
		}
		node_changed(idx);
		break;
	case GW_COMMAND_REMAINING_TIME_NTF:
		if (len != 6)
			goto invalid;
		break;

	/* operating mode */
	case GW_COMMAND_SEND_CFM:
	case GW_STATUS_REQUEST_CFM:
	case GW_WINK_SEND_CFM:
		if (len != 3)
			goto invalid;
		session = klf200_get16(dat);
		mylog(LOG_INFO, "session %u,%03x: %s", session, cmd, dat[2] ? "accept" : "reject");
		break;
	case GW_WINK_SEND_NTF:
	case GW_SESSION_FINISHED_NTF:
		if (len != 2)
			goto invalid;
		session = klf200_get16(dat);
		session_complete(session, cmd);
		break;
	/* dummy handlers */
	case GW_GET_MULTIPLE_ACTIVATION_LOG_LINES_CFM:
		if (len != 3)
			goto invalid;
		mylog(LOG_NOTICE, "log %s, %u lines", dat[2] ? "ok" : "fail", klf200_get16(dat));
		break;
	case GW_ACTIVATION_LOG_UPDATED_NTF:
		if (!log_req_scheduled++)
			libt_add_timeout(0.5, request_log, NULL);
		break;
	case GW_GET_MULTIPLE_ACTIVATION_LOG_LINES_NTF:
	case GW_GET_ACTIVATION_LOG_LINE_CFM:
		if (len != 17)
			goto invalid;
		time_t logtime = klf200_get32(dat);

		/* TODO: logs come in reverse order
		 * This method of removind duplicates does not work under load
		 */
		if (logtime < last_log_time)
			break;
		last_log_time = logtime;
		int setp = klf200_get16(dat+9);
		int loglevel = LOG_WARNING | LOG_MQTT;
		if (setp == 0xffff && dat[12] == 0x02)
			/* no real set, and no contact */
			loglevel = LOG_INFO | LOG_MQTT;
		session = klf200_get16(dat+4);
		mylog(loglevel, "on %s, session %u: %s set node %u.%u to %s: %s, %s, info %08x",
				klf200_timestr(klf200_get32(dat)),
				session,
				klf200_string(klf200_owner_str, dat[6]),
				dat[7], dat[8], klf200_decode_str(klf200_get16(dat+9)),
				klf200_string(klf200_runstatus_str, dat[11]),
				klf200_string(klf200_statusreply_str, dat[12]),
				klf200_get32(dat+13));

		if (dat[12] == 0x02) {
			/* no contact, replay */
			replay_session(session, dat[7]);
		}
		break;
	case GW_CS_ACTIVATE_CONFIGURATION_MODE_CFM:
		if (len != 79)
			goto invalid;
		klf200_log_node_masks(LOG_WARNING, dat, len,
				(const char *[]){ "activated", "no-contact", "error", }, 3);
		mylog(LOG_NOTICE, "%s %u", dat[78] ? "nok" : "ok", dat[78]);
		break;
	default:
invalid:
		mylog(LOG_WARNING, "recvd pkt %04x+%u", cmd, len);
		break;
	}
	session_confirmed(cmd, dat, len);
}
static void klf_recvd(int fd, void *dat)
{
	int cmd;

	if (klf200_recv(klfsock) < 0)
		return;
	for (;;) {
		cmd = klf200_next_packet(klfsock);
		if (cmd < 0)
			return;
		/* verify if complete packet has been received */
		if (cmd == GW_N_COMMAND)
			/* read more data ... */
			return;
		klf_pkt_recvd();
	}
}

static void timechanged(int fd, void *dat)
{
	libtimechange_iterate(fd);
	libtimechange_arm(fd);

	mylog(LOG_NOTICE, "sync time on change");
	klf200_put32(tdat, time(NULL));
	myklf200send(NULL, klfsock, GW_SET_UTC_REQ, tdat, 4, 1);
}

int main(int argc, char *argv[])
{
	int opt, ret;
	char *str;
	char **topics;

	/* argument parsing */
	while ((opt = getopt_long(argc, argv, optstring, long_opts, NULL)) >= 0)
	switch (opt) {
	case 'V':
		fprintf(stderr, "%s %s\nCompiled on %s %s\n",
				NAME, VERSION, __DATE__, __TIME__);
		exit(0);
	case '?':
		fputs(help_msg, stderr);
		exit(0);
	default:
		fprintf(stderr, "unknown option '%c'", opt);
		fputs(help_msg, stderr);
		exit(1);
		break;
	case 'v':
		++max_loglevel;
		break;
	case 'h':
		klfuri = optarg;
		break;
	case 'm':
		mqtt_host = optarg;
		str = strrchr(optarg, ':');
		if (str > mqtt_host && *(str-1) != ']') {
			/* TCP port provided */
			*str = 0;
			mqtt_port = strtoul(str+1, NULL, 10);
		}
		break;
	case 'p':
		mqtt_prefix = optarg;
		mqtt_prefix_len = strlen(mqtt_prefix);
		break;
	}

	if (!klfuri)
		mylog(LOG_ERR, "no host given, run with -h HOST");

	char *veluxpwd = NULL;
	if (lib_netrc(klfuri, NULL, &veluxpwd) < 0 || !veluxpwd)
		mylog(LOG_ERR, "no password found for %s in ~/.netrc", klfuri);

	asprintf(&addr_table_file, "%s/iohc-addr-table", getenv("HOME") ?: "/var/lib");

	/* MQTT start */
	mosquitto_lib_init();
	mosq = mosquitto_new(csprintf("klf200mqtt,%s,#%i", klfuri, getpid()), true, 0);
	if (!mosq)
		mylog(LOG_ERR, "mosquitto_new failed: %s", ESTR(errno));

	mosquitto_log_callback_set(mosq, my_mqtt_log);
	mosquitto_message_callback_set(mosq, my_mqtt_msg);

	ret = mosquitto_connect(mosq, mqtt_host, mqtt_port, mqtt_keepalive);
	if (ret)
		mylog(LOG_ERR, "mosquitto_connect %s:%i: %s", mqtt_host, mqtt_port, mosquitto_strerror(ret));

	/* subscribe to topics */
	topics = (optind >= argc) ? ((char *[]){ "#", NULL, }) : (argv+optind);
	for (; *topics; ++topics) {
		ret = mosquitto_subscribe(mosq, NULL, *topics, mqtt_qos);
		if (ret)
			mylog(LOG_ERR, "mosquitto_subscribe %s: %s", *topics, mosquitto_strerror(ret));
	}
	ret = mosquitto_subscribe(mosq, NULL, "config/"NAME"/#", mqtt_qos);
	if (ret)
		mylog(LOG_ERR, "mosquitto_subscribe config/"NAME"/#: %s", mosquitto_strerror(ret));

	libt_add_timeout(0, mqtt_maintenance, mosq);
	libe_add_fd(mosquitto_socket(mosq), recvd_mosq, mosq);

	/* prepare klf200 */
	mylog(LOG_NOTICE, "connect to klf200 on %s ...", klfuri);
	klfsock = klf200_connect(klfuri);
	libe_add_fd(klfsock, klf_recvd, NULL);

	mylog(LOG_INFO, "authenticate to klf200");
	char tdat[32];
	strncpy((char *)tdat, veluxpwd, 32);
	myklf200send(NULL, klfsock, GW_PASSWORD_ENTER_REQ, tdat, 32, 1);
	memset(veluxpwd, 0, strlen(veluxpwd));
	memset(tdat, 0, sizeof(tdat));
	free(veluxpwd);
	/* start ping repeat */
	myklf200_ping(NULL);
	/* queue initial commands */
	myklf200send(NULL, klfsock, GW_HOUSE_STATUS_MONITOR_ENABLE_REQ, NULL, 0, 3);
	myklf200send(NULL, klfsock, GW_GET_ALL_NODES_INFORMATION_REQ, NULL, 0, 0.5);
	memset(tdat, 0, 2);
	myklf200send(NULL, klfsock, GW_GET_ALL_GROUPS_INFORMATION_REQ, tdat, 2, 0.5);
	klf200_put32(tdat, time(NULL));
	myklf200send(NULL, klfsock, GW_SET_UTC_REQ, tdat, 4, 1);

	/* prepare signalfd */
	sigset_t sigmask;
	int sigfd;

	sigfillset(&sigmask);

	if (sigprocmask(SIG_BLOCK, &sigmask, NULL) < 0)
		mylog(LOG_ERR, "sigprocmask: %s", ESTR(errno));
	sigfd = signalfd(-1, &sigmask, SFD_NONBLOCK | SFD_CLOEXEC);
	if (sigfd < 0)
		mylog(LOG_ERR, "signalfd failed: %s", ESTR(errno));
	libe_add_fd(sigfd, signalrecvd, NULL);

	/* listen for time changes */
	int tfd;
	tfd = libtimechange_makefd();
	if (libtimechange_arm(tfd) < 0)
		mylog(LOG_ERR, "arm timechange fd failed: %s", ESTR(errno));
	libe_add_fd(tfd, timechanged, NULL);

	/* mark last time for log entries */
	time(&last_log_time);
	/* core loop */
	for (; !sigterm;) {
		libt_flush();
		mosq_update_flags();
		ret = libe_wait(libt_get_waittime());
		if (ret >= 0)
			libe_flush();
	}

	libe_remove_fd(klfsock);
	libe_remove_fd(tfd);
	close(klfsock);

	wipe_items_mqtt();

	/* terminate */
	send_self_sync(mosq, mqtt_qos);
	for (; !ready; ) {
		libt_flush();
		mosq_update_flags();
		ret = libe_wait(libt_get_waittime());
		if (ret >= 0)
			libe_flush();
	}
#if 1
	/* cleanup */
	mosquitto_disconnect(mosq);
	mosquitto_destroy(mosq);
	mosquitto_lib_cleanup();
#endif
	return 0;
}
