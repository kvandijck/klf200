#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <fnmatch.h>
#include <sys/stat.h>

#include "libtable.h"

#define xfree(x) if (x) free(x)

struct table {
	struct table *next;
	char *filename;
	time_t last_mtime;
	char **str;
	int nstr, sstr;
};
static struct table *tables;

static void table_reload(struct table *tbl)
{
	char *line = NULL;
	int j, ret;
	size_t linesize = 0;
	FILE *fp;
	char *key, *val;
	struct stat st;

	if (stat(tbl->filename, &st))
		return;

	if (st.st_mtime == tbl->last_mtime)
		/* no change */
		return;
	tbl->last_mtime = st.st_mtime;
	/* open netrc file */
	fp = fopen(tbl->filename, "r");
	if (!fp)
		return;

	for (j = 0; j < tbl->nstr; ++j)
		xfree(tbl->str[j]);
	tbl->nstr = 0;


	for (;;) {
		ret = getline(&line, &linesize, fp);
		if (ret <= 0)
			break;
		if (line[0] == '#')
			continue;
		if (tbl->nstr + 2 >= tbl->sstr) {
			tbl->sstr += 64;
			tbl->str = realloc(tbl->str, sizeof(*tbl->str) * tbl->sstr);
		}
		key = strtok(line, " \t\r\n\v\f");
		val = strtok(NULL, "\r\n\v\f");

		if (key) {
			tbl->str[tbl->nstr++] = strdup(key);
			tbl->str[tbl->nstr++] = strdup(val);
		}
	}

	fclose(fp);
	xfree(line);
}

static struct table *find_table(const char *filename)
{
	struct table *tbl;

	for (tbl = tables; tbl; tbl = tbl->next) {
		if (!strcmp(tbl->filename, filename))
			return tbl;
	}
	tbl = malloc(sizeof(*tbl));
	memset(tbl, 0, sizeof(*tbl));
	tbl->filename = strdup(filename);

	return tbl;
}

static int libtable_match_plain(const char *ref, const char *dut)
{
	return !strcmp(ref ?: "", dut);
}
static int libtable_match_wildcard(const char *ref, const char *dut)
{
	return !fnmatch(ref ?: "", dut, 0);
}

const char *libtable_lookup(const char *key, const char *filename, int flags)
{
	struct table *tbl;
	char **pstr;
	int (*match)(const char *ref, const char *dut);
	int swap;

	tbl = find_table(filename);
	table_reload(tbl);

	match = (flags & LTL_FNMATCH) ? libtable_match_wildcard : libtable_match_plain;
	swap = (flags & LTL_REVERSE) ? 1 : 0;

	for (pstr = tbl->str; pstr < tbl->str+tbl->nstr; pstr += 2) {
		if (match(pstr[0+swap] ?: "", key))
			return pstr[1-swap];
	}
	return NULL;
}
