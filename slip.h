#ifndef _slip_h_
#define _slip_h_

#ifdef __cplusplus
extern "C" {
#endif

/* slip:
 * encodes src into dst
 * return: encoded packet size, including FRAME END byte
 */
extern int slip(const void *vsrc, int len, void *vdst, int dstlen);

/* return length of 1 slip frame in src, including END byte
 * or -1 when no FRAME END found
 */
extern int slip_frame_len(const void *vsrc, int len);

/* unslip:
 * decodes src into dst
 * return: decoded packet size
 */
extern int unslip(const void *vsrc, int len, void *vdst, int dstlen);


#ifdef __cplusplus
}
#endif
#endif
