#include <errno.h>
#include <math.h>
#include <string.h>

#include <unistd.h>
#include <poll.h>
#include <syslog.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/conf.h>

#include "klf200.h"
#include "slip.h"
#include "lib/liburi.h"

#define ESTR(num)	strerror(num)
#define ESSL		ERR_error_string(ERR_get_error(), NULL)

static SSL_CTX *ctx;
/* only 1 ssl socket is required */
static SSL *ssl;
static int sock;

static void klf200_tracepkt(const char *msg, int sock, int protocol, int cmd, const void *payload, int len)
{
	static char buf[1024];
	char *str = buf;
	int j;
	const uint8_t *dat = payload;

	*str = 0;
	for (j = 0; j < len; ++j)
		str += sprintf(str, " %02x", dat[j]);

	mylog(LOG_DEBUG, "%s [%u] %u %04x+%u:%s", msg, sock, protocol, cmd, len, buf);
}

int klf200_connect(const char *url)
{
	int ret;
	struct addrinfo *resolved, *h;
	struct uri uri = {};

	lib_parse_uri(url, &uri);

	/* resolve */
	char service[32];
	sprintf(service, "%i", uri.port ?: 51200);
	struct addrinfo hints = {
		.ai_family = AF_UNSPEC,
		.ai_socktype = SOCK_STREAM,
	};
	ret = getaddrinfo(uri.host, service, &hints, &resolved);
	if (ret)
		mylog(LOG_ERR, "could not resolve host '%s': %s", uri.host, gai_strerror(ret));

	lib_clean_uri(&uri);
	if (!resolved)
		mylog(LOG_ERR, "no host defined");

	if (!ctx) {
		/* start SSL ctx */
		SSL_library_init();
		SSL_load_error_strings();
		OPENSSL_config(NULL);

		ctx = SSL_CTX_new(SSLv23_method());
		if (!ctx)
			mylog(LOG_ERR, "no ssl context: %s", ESSL);
		SSL_CTX_set_verify(ctx, SSL_VERIFY_NONE, NULL);
		SSL_CTX_set_options(ctx, SSL_OP_NO_COMPRESSION);
	}

	/* prepare ssl part */
	ssl = SSL_new(ctx);
	if (!ssl)
		mylog(LOG_ERR, "no ssl session: %s", ESSL);

	for (h = resolved; h; h = h->ai_next) {
		/* create socket */
		sock = socket(h->ai_family, h->ai_socktype, h->ai_protocol);
		if (sock < 0) {
			mylog(LOG_WARNING, "socket: %s", ESTR(errno));
			continue;
		}

		if (connect(sock, h->ai_addr, h->ai_addrlen) < 0) {
			mylog(LOG_WARNING, "connect %s: %s", url, ESTR(errno));
			close(sock);
			continue;
		}
		break;
	}
	freeaddrinfo(resolved);
	if (!h)
		mylog(LOG_ERR, "no connection to %s", url);

	/* ssl init */
	if (!SSL_set_fd(ssl, sock))
		mylog(LOG_ERR, "assign socket to SSL: %s", ESSL);
	if (!SSL_connect(ssl))
		mylog(LOG_ERR, "ssl connect: %s", ESSL);

	return sock;
}

void klf200_cleanup(void)
{
	SSL_free(ssl);
	SSL_CTX_free(ctx);
	CONF_modules_free();
	ERR_free_strings();
	close(sock);
}

static uint8_t rbuf[1024];
static int rpos, rfill, rsize = sizeof(rbuf);

int klf200_recv(int sock)
{
	struct pollfd pfd = { .fd = sock, .events = POLLIN, };
	int ret;

	if (rpos > 0) {
		/* move pending buffer to the start */
		if (rpos < rfill)
			memmove(rbuf, rbuf+rpos, rfill - rpos);
		rfill -= rpos;
		rpos = 0;
	}
	if (rfill >= rsize)
		mylog(LOG_ERR, "klf200 buffer filled");

	ret = poll(&pfd, 1, 0);
	if (ret > 0) {
		ret = SSL_read(ssl, rbuf+rfill, rsize-rfill);
		if (ret < 0)
			mylog(LOG_WARNING, "SSL_read: %s", ESSL);
		if (ret > 0)
			rfill += ret;
	}
	return ret;
}

static uint8_t rpkt[256];
static int rpktlen;

static uint8_t klf200_checksum(const void *vdat, int len)
{
	const uint8_t *dat = vdat;
	uint8_t sum;

	for (sum = *dat++, --len; len; --len, ++dat)
		sum ^= *dat;
	return sum;
}

int klf200_pktlen(void)
{
	if (rpktlen < 5)
		return -1;
	return rpktlen-5;
}

const void *klf200_pktdat(void)
{
	if (rpktlen < 5)
		return NULL;
	return rpkt+4;
}

int klf200_pktcmd(void)
{
	if (rpktlen < 5)
		return -1;
	return klf200_get16(rpkt+2);
}

int klf200_next_packet(int sock)
{
	int rawlen;

	for (;;) {
		rawlen = slip_frame_len(rbuf+rpos, rfill-rpos);
		if (rawlen < 0)
			return GW_N_COMMAND;
		rpktlen = unslip(rbuf+rpos, rawlen, rpkt, sizeof(rpkt));
		if (rpktlen < 0)
			mylog(LOG_ERR, "unslip() failed");
		/* raw length bytes consumed */
		rpos += rawlen;
		if (rpktlen > 0) {
			/* only return non-empty slip packets
			 * it would be hard to differ empty packet
			 * from NO-MORE-DATA
			 */
			if (klf200_checksum(rpkt, rpktlen-1) != rpkt[rpktlen-1])
				mylog(LOG_ERR, "klf200 checksump failure");
			if (rpkt[0] != 0)
				mylog(LOG_ERR, "klf200 protocol id %u unknown", rpkt[0]);
			if (rpkt[1] != rpktlen-2)
				mylog(LOG_ERR, "klf200 pktlen mismatch");
			klf200_tracepkt("<", sock, rpkt[0], klf200_pktcmd(), klf200_pktdat(), klf200_pktlen());
			return klf200_pktcmd();
		}
	}
}

/* utils */
int klf200_entries_to_come(void)
{
	if (klf200_pktcmd() == GW_CS_GET_SYSTEMTABLE_DATA_NTF)
		return ((const uint8_t *)klf200_pktdat())[klf200_pktlen()-1];
	else
		return 0;
}

/* transmit */
static uint8_t tbuf[256], tenc[512];

int klf200_send(int sock, int proto, int cmd, const void *vdat, int len)
{
	int rawlen, ret;

	/* TODO */
	if (len > 250)
		mylog(LOG_ERR, "klf200_send %u: too big", len);
	klf200_tracepkt(">", sock, proto, cmd, vdat, len);

	tbuf[0] = proto;
	tbuf[1] = len+3;
	klf200_put16(tbuf+2, cmd);
	memcpy(tbuf+4, vdat, len);
	tbuf[len+4] = klf200_checksum(tbuf, len+4);

	rawlen = slip(tbuf, 5+len, tenc, sizeof(tenc));
	if (rawlen < 0)
		mylog(LOG_ERR, "slip() failed");
	ret = SSL_write(ssl, tenc, rawlen);
	if (ret < 0)
		mylog(LOG_WARNING, "SSL_write: %s", ESSL);

	return ret;
}

static int last_klf200_sessionid = 0;

int klf200_sessionid(void)
{
	++last_klf200_sessionid;
	/* avoid rollover and have 0 */
	if (!last_klf200_sessionid)
		++last_klf200_sessionid;
	return last_klf200_sessionid;
}

/* values */
uint16_t klf200_encode_abs(double val)
{
	if (val < 0)
		val = 0;
	else if (val > 1)
		val = 1;
	return lround(val * 0xc800);
}

uint16_t klf200_encode_delta(double val)
{
	if (val < -1)
		val = -1;
	else if (val > 1)
		val = 1;
	return lround((val * 1000) + 0xc900 + 1000);
}

int klf200_rawvalue_is_abs(uint16_t value)
{
	return value <= 0xc800;
}

int klf200_rawvalue_is_delta(uint16_t value)
{
	return value >= 0xc900 && value <= 0xd0d0;
}

double klf200_decode_abs(uint16_t value)
{
	return value * 1.0 / 0xc800;
}

double klf200_decode_delta(uint16_t value)
{
	return value*1.0-(0xc900+1000) / 1000;
}

uint16_t klf200_encode_str(const char *str, char **pendp)
{
	char *lendp;
	double val;

	if (!pendp)
		pendp = &lendp;

	if (!strncmp(str, "target", 7)) {
		*pendp = (char *)str+7;
		return 0xd100;
	} else if (!strncmp(str, "curr", 4) || !strncmp(str, "stop", 4)) {
		*pendp = (char *)str+4;
		return 0xd200;
	} else if (!strncmp(str, "default", 7)) {
		*pendp = (char *)str+7;
		return 0xd300;
	} else if (!strncmp(str, "nan", 3)) {
		*pendp = (char *)str+3;
		return 0xd400;
	} else if (!strcmp(str, "")) {
		*pendp = (char *)str;
		return 0xd400;
	} else if (*str == '+' || *str == '-') {
		val = strtod(str, pendp);
		return klf200_encode_delta(val);
	} else {
		val = strtod(str, pendp);
		return klf200_encode_abs(val);
	}
}

const char *klf200_dtostr(double d, const char *fmt)
{
	static char buf[64];
	char *str;
	int ptpresent = 0;

	if (isnan(d))
		return "";
	sprintf(buf, fmt ?: "%lg", d);
	for (str = buf; *str; ++str) {
		if (*str == '.')
			ptpresent = 1;
		else if (*str == 'e')
			/* nothing to do anymore */
			break;
		else if (ptpresent && *str == '0') {
			int len = strspn(str, "0");
			if (!str[len]) {
				/* entire string (after .) is '0' */
				*str = 0;
				if (str > buf && *(str-1) == '.')
					/* remote '.' too */
					*(str-1) = 0;
				break;
			}
		}
	}
	return buf;
}
const char *klf200_decode_str(uint16_t value)
{
	static char str[32][32];
	static int idx;
	static char thisstr[32];
	const char *result;

	if (klf200_rawvalue_is_abs(value))
		result = klf200_dtostr(klf200_decode_abs(value), NULL);

	else if (klf200_rawvalue_is_delta(value))
		result = klf200_dtostr(klf200_decode_delta(value), "%+lg");

	else if (value == 0xd100)
		return "target";

	else if (value == 0xd200)
		return "curr";

	else if (value == 0xd300)
		return "default";

	else if (value == 0xf7ff)
		return "";

	else {
		sprintf(thisstr, "%04x", value);
		result = thisstr;
	}

	idx = (idx+1) % 16;
	strcpy(str[idx], result);
	return str[idx];
}

const char *klf200_timestr(time_t tim)
{
	static char timbuf[32];

	strftime(timbuf, sizeof(timbuf), "%Y%m%dT%H%M%S", localtime(&tim));
	return timbuf;
}

int klf200_nodestrtobitmask(const char *str, void *vdat, int len)
{
	int value, n;
	uint8_t *dat = vdat;

	memset(dat, 0, len);
	for (n = 0; n < len && *str; ++n) {
		value = 0;
		/* strtoul from const */
		for (; *str && strchr("0123456789", *str); ++str)
			value = value*10 + (*str - '0');
		if (*str)
			++str;
		/* put in mask */
		dat[value/8] |= 1 << (value % 8);
	}
	return n;
}

int klf200_nodestrtolist(const char *str, void *vdat, int len)
{
	int value, n;
	uint8_t *dat = vdat;

	memset(dat, 0, len);
	for (n = 0; n < len && *str; ++n) {
		value = 0;
		/* strtoul from const */
		for (; *str && strchr("0123456789", *str); ++str)
			value = value*10 + (*str - '0');
		if (*str)
			++str;
		/* put in list */
		dat[n] = value;
	}
	return n;
}

void klf200_log_node_masks(int loglevel, const void *vdat, int len,
		const char *const *str, int nstr)
{
	const uint8_t *dat = vdat;
	int node, set;
	const char *result;

	/* ceil #options */
	if (nstr > len/26)
		nstr = len/26;
	for (node = 0; node < 204; ++node) {
		result = NULL;
		for (set = 0; set < nstr; ++set) {
			if ((dat[set*26+node/8] >> (node % 8)) & 1) {
				result = str[set];
				break;
			}
		}
		if (result)
			mylog(loglevel, "%s.%u: %s", (node < 200) ? "node" : "beacon", node % 200, result);
	}
}
