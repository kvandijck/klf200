# klf200cli & klf200mqtt

This project provides an interface to the klf200 box, using it's binary protocol.

klf200 is a gateway to IO-HomeControl nodes, without requiring a cloud service.
That makes the klf200 interesting to use in local home automation project when
interfacing to IO-HomeControl nodes is required.

## klf200cli

running `klf200cli -?` shows a more-or-less complete usage instructions

klf200cli allows to experiment with the commands that you can send to IO-HomeControl
nodes.

## klf200mqtt

This is the main gateway towards IO-HomeControl via the klf200 box.
It reads the klf200 nodes, and exposes them in MQTT topics.

It also allows you to create ad-hoc groups of nodes in a single topic, which will
be controlled with custom *originator*, *priority* & *lock* values.

## note on password

klf200 requires a password to connect to.

both `klf200cli` and `klf200mqtt` get the password from a `~/.netrc` file.

A typical `~/.netrc` file would have a single line for your klf200 host:

	machine <YOUR_KLF200_HOST> password <YOUR_PASSWORD>

see also `man 5 netrc`
