#include <stdio.h>

#include "klf200.h"

const char *const klf200_node_type_str[256] = {
	[0] = "NO_TYPE",  // (All nodes except controller)
	[1] = "Venetian blind",
	[2] = "Roller shutter",
	[3] = "Awning",  // (External for windows)
	[4] = "Window opener",
	[5] = "Garage opener",
	[6] = "Light",
	[7] = "Gate opener",
	[8] = "Rolling Door Opener",
	[9] = "Lock",
	[10] = "Blind",
	[11] = "SCD",  // (Secure Configuration Device)
	[12] = "Beacon",
	[13] = "Dual Shutter",
	[14] = "Heating Temperature Interface",
	[15] = "On / Off Switch",
	[16] = "Horizontal Awning",
	[17] = "External Venetian Blind",
	[18] = "Louvre Blind",
	[19] = "Curtain track",
	[20] = "Ventilation Point",
	[21] = "Exterior heating",
	[22] = "Heat pump",  // (Not currently supported)
	[23] = "Intrusion alarm",
	[24] = "Swinging Shutter",
};

const char *const klf200_prio_level_str[8] = {
	[0] = "human",
	[1] = "environment",
	[2] = "user1",
	[3] = "user2",
	[4] = "comfort1",
	[5] = "comfort2",
	[6] = "comfort3",
	[7] = "comfort4",
};

const char *const klf200_owner_str[256] = {
	[0x00] = "local",  // User pressing button locally on actuator
	[0x01] = "user", // User Remote control causing action on actuator
	[0x02] = "rain", // Sensor
	[0x03] = "timer", // Sensor
	[0x04] = "security", // SCD controlling actuator
	[0x05] = "ups", // UPS unit
	[0x06] = "sfc", // Smart Function Controller
	[0x07] = "lsc", // Lifestyle Scenario Controller
	[0x08] = "program", // Stand Alone Automatic Controls
	[0x09] = "wind", // Wind detection
	[0x0a] = "self", // Used when an actuator decides to move by itself
	[0x0b] = "loadshed",
	[0x0c] = "light", // light sensor
	[0x0d] = "sensor", // unspecified environmental sensor
	[0x11] = "cycle", // Used in context with automatic cycle;
	[0xff] = "emergency" // Used in context with emergency or security commands,
   // -this command originator should never be disabled
};

const char *const klf200_velocity_str[256] = {
	[0] = "normal",
	[1] = "silent",
	[2] = "fast",
	[255] = "NA", //Only used in status reply
};

const char *const klf200_copymode_str[256] = {
	[0] = "TransmittingConfigurationMode", //Transmitting Configuration Mode (TCM): The gateway gets key and system table from another controller.
	[1] = "ReceivingConfigurationMode",    //Receiving Configuration Mode (RCM): The gateway gives key and system table to another controller.
};

const char *const klf200_error_str[256] = {
	[0] = "Not further defined error",
	[1] = "Unknown command or command is not accepted in this state",
	[2] = "Error on frame structure",
	[7] = "Busy, try again later",
	[8] = "Bad system table index",
	[12] = "Not authenticated",
};

const char *const klf200_state_str[256] = {
	[0] = "Test mode",
	[1] = "Gateway mode, empty table",
	[2] = "Gateway mode, non-empty table",
	[3] = "Beacon mode, not configured by a remote",
	[4] = "Beacon mode, configured by a remote",
};

const char *const klf200_substate_str[256] = {
	[0x00] = "idle",
	[0x01] = "performing task in configuration service handler",
	[0x02] = "performing scene configuration",
	[0x03] = "performing information service configuration",
	[0x04] = "performing contact input configuration",

	[0x80] = "performing task in command handler",
	[0x81] = "performing task in activate group handler",
	[0x82] = "performing task in activate scene handler",
};

const char *const klf200_nodestate_str[256] = {
	[0] = "idle",
	[1] = "error",
	[3] = "wait-for-power",
	[4] = "executing",
	[5] = "done",
	[255] = "unknown",
};

const char *const klf200_manufacturer_str[1024] = {
	[1] = "Velux",
	[2] = "Somfy",
	[3] = "Honeywell",
	[4] = "Hörmann",
	[5] = "Assa Abloy",
	[6] = "Niko",
	[7] = "Window Master",
	[8] = "Renson",
	[9] = "CIAT",
	[10] = "Secuyou",
	[11] = "Overkiz",
	[12] = "Atlantic Group",
};

const char *const klf200_grouptype_str[256] = {
	[0] = "user",
	[1] = "room",
	[2] = "house",
	[3] = "allgroup",
};

const char *const klf200_runstatus_str[256] = {
	[0] = "complete",
	[1] = "failed",
	[2] = "pending",
};

const char *const klf200_statusreply_str[256] = {
	[0x00] = "unknown",
	[0x01] = "ok",
	[0x02] = "no contact",
	[0x03] = "manually operated",
	[0x04] = "blocked",
	[0x05] = "wrong systemkey",
	[0x06] = "priority level locked",
	[0x07] = "reached wrong position",
	[0x08] = "error during execution",
	[0x09] = "no execution",
	[0x0a] = "calibrating",
	[0x0b] = "power too hi",
	[0x0c] = "power too lo",
	[0x0d] = "lock position open",
	[0x0e] = "motion timeout",
	[0x0f] = "thermal protection",
	[0x10] = "not operational",
	[0x11] = "requires maintenance",
	[0x12] = "battery level",
	[0x13] = "target modified",
	[0x14] = "mode not implemented",
	[0x15] = "incompatible movement",
	[0x16] = "user action",
	[0x17] = "dead-bolt error",
	[0x18] = "automatic cycle",
	[0x19] = "wrong load",
	[0x1a] = "color unreachable",
	[0x1b] = "target unreachable",
	[0x1c] = "bad index",
	[0x1d] = "overruled",
	[0x1e] = "wait for power",
	[0xdf] = "unknown code", /* look at code */
	[0xe0] = "parameter limited",
	[0xe1] = "limit by local",
	[0xe2] = "limit by user",
	[0xe3] = "limit by rain",
	[0xe4] = "limit by timer",
	[0xe6] = "limit by ups",
	[0xe7] = "limit by unknown",
	[0xea] = "limit by program",
	[0xeb] = "limit by wind",
	[0xec] = "limit by self",
	[0xed] = "limit by cycle",
	[0xee] = "limit by emergency",
};

const char *const klf200_statustype_str[256] = {
	[0] = "setpoint",
	[1] = "value",
	[2] = "time",
	[3] = "main",
};

const char *klf200_string(const char *const *table, int idx)
{
	const char *ret;

	ret = table[idx];
	if (!ret) {
		static char buf[16];

		sprintf(buf, "%u", idx);
		ret = buf;
	}
	return ret;
}

int klf200_string_idx(const char *needle, const char *const *table, int len)
{
	int j;

	for (j = 0; j < len; ++j) {
		if (table[j] && !strcmp(needle, table[j]))
			return j;
	}
	return -1;
}
