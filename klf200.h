#include <stdint.h>
#include <string.h>
#include <time.h>

#include <endian.h>

#ifndef _klf200_h_
#define _klf200_h_

#ifdef __cplusplus
extern "C" {
#endif

__attribute__((format(printf,2,3)))
extern void mylog(int loglevel, const char *fmt, ...);


extern int klf200_connect(const char *str);
extern void klf200_cleanup(void);

extern int klf200_send(int sock, int proto, int cmd, const void *vdat, int len);
extern int klf200_recv(int sock);

extern int klf200_next_packet(int sock);
extern int klf200_pktcmd(void);
extern const void *klf200_pktdat(void);
extern int klf200_pktlen(void);

/* sessions */
extern int klf200_sessionid(void);
/* for system table */
extern int klf200_entries_to_come(void);

/* endianness */
static inline int klf200_get16(const void *dat)
{
	uint16_t v;

	memcpy(&v, dat, sizeof(v));
	return be16toh(v);
}
static inline int klf200_get32(const void *dat)
{
	uint32_t v;

	memcpy(&v, dat, sizeof(v));
	return be32toh(v);
}
static inline uint64_t klf200_get64(const void *dat)
{
	uint64_t v;

	memcpy(&v, dat, sizeof(v));
	return be64toh(v);
}
static inline void klf200_put16(void *dat, uint16_t v)
{
	v = htobe16(v);
	memcpy(dat, &v, sizeof(v));
}
static inline void klf200_put32(void *dat, uint32_t v)
{
	v = htobe32(v);
	memcpy(dat, &v, sizeof(v));
}
static inline void klf200_put64(void *dat, uint64_t v)
{
	v = htobe64(v);
	memcpy(dat, &v, sizeof(v));
}

static inline int klf200_get24(const void *dat)
{
	uint32_t v = 0;

	memcpy(&v, dat, 3);
	return be32toh(v) >> 8;
}
static inline void klf200_put24(void *dat, uint32_t v)
{
	v = htobe32(v << 8);
	memcpy(dat, &v, 3);
}

/* values coding */
extern const char *klf200_dtostr(double d, const char *fmt);
extern uint16_t klf200_encode_abs(double val);
extern uint16_t klf200_encode_delta(double val);
extern uint16_t klf200_encode_str(const char *str, char **pendp);

extern int klf200_rawvalue_is_abs(uint16_t value);
extern int klf200_rawvalue_is_delta(uint16_t value);
extern double klf200_decode_abs(uint16_t value);
extern double klf200_decode_delta(uint16_t value);
extern const char *klf200_decode_str(uint16_t value);

extern const char *klf200_timestr(time_t tim);

extern int klf200_nodestrtobitmask(const char *str, void *vdat, int len);
extern int klf200_nodestrtolist(const char *str, void *vdat, int len);

extern void klf200_log_node_masks(int loglevel, const void *vdat, int len,
		const char *const *strtable, int nstrtable);

/* string tables */
extern const char *const klf200_node_type_str[256];
extern const char *const klf200_grouptype_str[256];
extern const char *const klf200_prio_level_str[8];
extern const char *const klf200_owner_str[256];
extern const char *const klf200_velocity_str[256];
extern const char *const klf200_copymode_str[256];
extern const char *const klf200_error_str[256];
extern const char *const klf200_state_str[256];
extern const char *const klf200_substate_str[256];
extern const char *const klf200_nodestate_str[256];
extern const char *const klf200_manufacturer_str[1024];
extern const char *const klf200_runstatus_str[256];
extern const char *const klf200_statusreply_str[256];
extern const char *const klf200_statustype_str[256];

extern const char *klf200_string(const char *const *table, int idx);
extern int klf200_string_idx(const char *needle, const char *const *table, int len);
/* commands */
#define GW_ERROR_NTF  0x0000

#define GW_REBOOT_REQ	0x0001
#define GW_REBOOT_CFM	0x0002
#define GW_SET_FACTORY_DEFAULT_REQ	0x0003
#define GW_SET_FACTORY_DEFAULT_CFM	0x0004

#define GW_GET_VERSION_REQ	0x0008
#define GW_GET_VERSION_CFM	0x0009
#define GW_GET_PROTOCOL_VERSION_REQ	0x000a
#define GW_GET_PROTOCOL_VERSION_CFM	0x000b
#define GW_GET_STATE_REQ	0x000c
#define GW_GET_STATE_CFM	0x000d
#define GW_LEAVE_LEARN_STATE_REQ	0x000e
#define GW_LEAVE_LEARN_STATE_CFM	0x000f

#define GW_GET_NETWORK_REQ	0x00e0
#define GW_GET_NETWORK_CFM	0x00e1
#define GW_SET_NETWORK_REQ	0x00e2
#define GW_SET_NETWORK_CFM	0x00e3

#define GW_CS_GET_SYSTEMTABLE_DATA_REQ	0x0100
#define GW_CS_GET_SYSTEMTABLE_DATA_CFM	0x0101
#define GW_CS_GET_SYSTEMTABLE_DATA_NTF	0x0102
#define GW_CS_DISCOVER_NODES_REQ	0x0103
#define GW_CS_DISCOVER_NODES_CFM	0x0104
#define GW_CS_DISCOVER_NODES_NTF	0x0105
#define GW_CS_REMOVE_NODES_REQ		0x0106
#define GW_CS_REMOVE_NODES_CFM		0x0107
#define GW_CS_VIRGIN_STATE_REQ		0x0108
#define GW_CS_VIRGIN_STATE_CFM		0x0109
#define GW_CS_CONTROLLER_COPY_REQ	0x010a
#define GW_CS_CONTROLLER_COPY_CFM	0x010b
#define GW_CS_CONTROLLER_COPY_NTF	0x010c
#define GW_CS_CONTROLLER_COPY_CANCEL_NTF 0x010d
#define GW_CS_RECEIVE_KEY_REQ		0x010e
#define GW_CS_RECEIVE_KEY_CFM		0x010f
#define GW_CS_RECEIVE_KEY_NTF		0x0110
#define GW_CS_PGC_JOB_NTF		0x0111
#define GW_CS_SYSTEM_TABLE_UPDATE_NTF	0x0112
#define GW_CS_GENERATE_NEW_KEY_REQ	0x0113
#define GW_CS_GENERATE_NEW_KEY_CFM	0x0114
#define GW_CS_GENERATE_NEW_KEY_NTF	0x0115
#define GW_CS_REPAIR_KEY_REQ		0x0116
#define GW_CS_REPAIR_KEY_CFM		0x0117
#define GW_CS_REPAIR_KEY_NTF		0x0118
#define GW_CS_ACTIVATE_CONFIGURATION_MODE_REQ 0x0119
#define GW_CS_ACTIVATE_CONFIGURATION_MODE_CFM 0x011a

#define GW_GET_NODE_INFORMATION_REQ	0x0200
#define GW_GET_NODE_INFORMATION_CFM	0x0201
#define GW_GET_NODE_INFORMATION_NTF	0x0210
#define GW_GET_ALL_NODES_INFORMATION_REQ	0x0202
#define GW_GET_ALL_NODES_INFORMATION_CFM	0x0203
#define GW_GET_ALL_NODES_INFORMATION_NTF	0x0204
#define GW_GET_ALL_NODES_INFORMATION_FINISHED_NTF 0x0205
#define GW_SET_NODE_NAME_REQ		0x0205
#define GW_SET_NODE_NAME_CFM		0x0206
#define GW_SET_NODE_NAME_NTF		0x0207
#define GW_SET_NODE_VELOCITY_REQ		0x020a
#define GW_SET_NODE_VELOCITY_CFM		0x020b
#define GW_NODE_INFORMATION_CHANGED_NTF		0x020c
#define GW_NODE_STATE_POSITION_CHANGED_NTF	0x0211

#define GW_GET_GROUP_INFORMATION_REQ		0x0220
#define GW_GET_GROUP_INFORMATION_CFM		0x0221
#define GW_GET_GROUP_INFORMATION_NTF		0x0230
#define GW_SET_GROUP_INFORMATION_REQ		0x0222
#define GW_SET_GROUP_INFORMATION_CFM		0x0223
#define GW_GROUP_INFORMATION_CHANGED_NTF	0x0224
#define GW_DELETE_GROUP_REQ			0x0225
#define GW_DELETE_GROUP_CFM			0x0226
#define GW_NEW_GROUP_REQ			0x0227
#define GW_NEW_GROUP_CFM			0x0228
#define GW_GET_ALL_GROUPS_INFORMATION_REQ	0x0229
#define GW_GET_ALL_GROUPS_INFORMATION_CFM	0x022a
#define GW_GET_ALL_GROUPS_INFORMATION_NTF	0x022b
#define GW_GET_ALL_GROUPS_INFORMATION_FINISHED_NTF 0x022c
#define GW_GROUP_DELETED_CFM			0x022d

#define GW_HOUSE_STATUS_MONITOR_ENABLE_REQ 0x0240
#define GW_HOUSE_STATUS_MONITOR_ENABLE_CFM 0x0241
#define GW_HOUSE_STATUS_MONITOR_DISABLE_REQ 0x0242
#define GW_HOUSE_STATUS_MONITOR_DISABLE_CFM 0x0243

#define GW_COMMAND_SEND_REQ		0x0300
#define GW_COMMAND_SEND_CFM		0x0301
#define GW_COMMAND_RUN_STATUS_NTF	0x0302
#define GW_COMMAND_REMAINING_TIME_NTF	0x0303
#define GW_SESSION_FINISHED_NTF		0x0304
#define GW_STATUS_REQUEST_REQ		0x0305
#define GW_STATUS_REQUEST_CFM		0x0306
#define GW_STATUS_REQUEST_NTF		0x0307
#define GW_WINK_SEND_REQ		0x0308
#define GW_WINK_SEND_CFM		0x0309
#define GW_WINK_SEND_NTF		0x030a

#define GW_ACTIVATE_PRODUCTGROUP_REQ	0x0447
#define GW_ACTIVATE_PRODUCTGROUP_CFM	0x0448
#define GW_ACTIVATE_PRODUCTGROUP_NTF	0x0449

#define GW_GET_ACTIVATION_LOG_HEADER_REQ		0x0500
#define GW_GET_ACTIVATION_LOG_HEADER_CFM		0x0501
#define GW_CLEAR_ACTIVATION_LOG_REQ			0x0502
#define GW_CLEAR_ACTIVATION_LOG_CFM			0x0503
#define GW_GET_ACTIVATION_LOG_LINE_REQ			0x0504
#define GW_GET_ACTIVATION_LOG_LINE_CFM			0x0505
#define GW_ACTIVATION_LOG_UPDATED_NTF			0x0506
#define GW_GET_MULTIPLE_ACTIVATION_LOG_LINES_REQ	0x0507
#define GW_GET_MULTIPLE_ACTIVATION_LOG_LINES_NTF	0x0508
#define GW_GET_MULTIPLE_ACTIVATION_LOG_LINES_CFM	0x0509

#define GW_SET_UTC_REQ			0x2000
#define GW_SET_UTC_CFM			0x2001
#define GW_RTC_SET_TIME_ZONE_REQ	0x2002
#define GW_RTC_SET_TIME_ZONE_CFM	0x2003
#define GW_GET_LOCAL_TIME_REQ		0x2004
#define GW_GET_LOCAL_TIME_CFM		0x2005

#define GW_PASSWORD_ENTER_REQ  0x3000
#define GW_PASSWORD_ENTER_CFM  0x3001

#define GW_PASSWORD_CHANGE_REQ  0x3002
#define GW_PASSWORD_CHANGE_CFM  0x3003
#define GW_PASSWORD_CHANGED_NTF  0x3004

#define GW_N_COMMAND 0x10000

#ifdef __cplusplus
}
#endif
#endif
