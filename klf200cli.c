#include <errno.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>

#include <unistd.h>
#include <getopt.h>
#include <poll.h>
#include <syslog.h>
#include <sys/uio.h>

#include "lib/libnetrc.h"
#include "lib/libtable.h"
#include "klf200.h"

#define NAME "klf200cli"
#ifndef VERSION
#define VERSION "<undefined version>"
#endif

static int max_loglevel = LOG_WARNING;
static int logtime;
/* generic error logging */
__attribute((format(printf,2,3)))
void mylog(int loglevel, const char *fmt, ...)
{
	va_list va;
	char *str;
	struct timespec tv;
	char timbuf[64];

	if (loglevel <= max_loglevel) {
		if (logtime) {
			clock_gettime(CLOCK_REALTIME, &tv);
			strftime(timbuf, sizeof(timbuf), "%b %d %H:%M:%S", localtime(&tv.tv_sec));
			sprintf(timbuf+strlen(timbuf), ".%03u ", (int)(tv.tv_nsec/1000000));
		} else {
			/* make it empty */
			timbuf[0] = 0;
		}

		va_start(va, fmt);
		vasprintf(&str, fmt, va);
		va_end(va);

		struct iovec vec[] = {
			{ .iov_base = timbuf, .iov_len = strlen(timbuf), },
			{ .iov_base = NAME, .iov_len = strlen(NAME), },
			{ .iov_base = ": ", .iov_len = 2, },
			{ .iov_base = str, .iov_len = strlen(str), },
			{ .iov_base = "\n", .iov_len = 1, },
		};
		writev(STDERR_FILENO, vec, sizeof(vec)/sizeof(vec[0]));
		free(str);
	}
	if (loglevel <= LOG_ERR)
		exit(1);
}

#define ESTR(num)	strerror(num)
#define ESSL		ERR_error_string(ERR_get_error(), NULL)

/* program options */
static const char help_msg[] =
	NAME ": KLF200 IO-homecontrol commandline tool\n"
	"usage:	" NAME " [OPTIONS ...]\n"
	"       " NAME " [OPTIONS ...] COMMAND ...\n"
	"\n"
	"Commands\n"
	" reboot\n"
	" factory		Set factory defaults\n"
	" stoplearn		Leave learning state\n"
	" pwd			Set new password\n"
	" network [?|dhcp]	Get/Set network (only DHCP supported now)\n"
	" time [?|sync]		Get time, or set from host's time\n"
	" table			List 'system table'\n"
	" discover		Discover IO-HomeControl nodes\n"
	" remove id[,id2...]	Remove indexed nodes\n"
	" config id[,id2...]	Open indexed nodes for configuration\n"
	" virgin		Return to virgin state\n"
	" copy get		Get controller copy from remote\n"
	"      give		Give controller copy to remote\n"
	"      cancel		Cancel pending 'give controller copy'\n"
	" key new		Generate new key\n"
	"     recv		Receive key\n"
	"     repair		Repair/distribute key\n"
	" monitor [on|off]	Enable/disable House Monitor Mode\n"
	" node id		Get indexed node info\n"
	"      all		Get all nodes info\n"
	"      name id NAME	Change node name for indexed node\n"
	"      velocity id VEL	Change node velocity for indexed node\n"
	" group id		Get indexed group info\n"
	"       all		Get all groups info\n"
	"	add PROP=VALUE ...	Add new group with properties\n"
	"	modify id PROP=VALUE ...	Modify group properties\n"
	"	  name		Group name\n"
	"	  type		(user|room|house)\n"
	"	  nodes		\n"
	"	  velocity	(normal|silent|fast)\n"
	"       rm id		Remove a group\n"
	"	set id VALUE PROP=VALUE ...	Change actuators in a group\n"
	"	  from=ORIGINATOR\n"
	"	  prio=PRIO\n"
	"	  session=IDX\n"
	"	  velocity=VEL	(normal|silent|fast)\n"
	"	  value=V	Any of (*val|fp1|fp2|...|fp16)\n"
	" log stat		Show log statistics\n"
	"     clear		Clear log\n"
	"     all		Show all log\n"
	" set PROP=VALUE ...	Change value\n"
	"	val=VALUE	Change main value\n"
	"	fp1=VALUE	Change FP1\n"
	"	fp2=VALUE	Change FP2\n"
	"	...\n"
	"	fp16=VALUE	Change FP16\n"
	"	from=ORIGINATOR	(local|*user|rain|timer|security|ups|sfc|lsc|program|...\n"
	"			wind|self|cycle|emergency)\n"
	"	prio=PRIO	(humanprot|envprot|user1|*user2|comfort(1..4))\n"
	"	nodes=ID,...	Nodes to set at once (max 20)\n"
	"	session=IDX	Overrule automatic session number\n"
	"	lock=PRIO	Add lock for PRIO\n"
	"	unlock=PRIO	Add unlock for PRIO\n"
	"	locktime=VALUE	Set lock/unlock time (0=30s, 1=1m, 2=1m30, 3=3m, 254=127m30s, 255=forever)\n"
	" status PROP=VALUE ...	Request status\n"
	"	nodes=ID,...	Nodes to set at once (max 20)\n"
	"	session=IDX	Overrule automatic session number\n"
	"	type=TYPE	(setpoint|value|time|*main)\n"
	"	values=V[,..]	List of (*val|fp1|fp2|...|fp16)\n"
	" wink PROP=VLAUE ...	Wink nodes\n"
	"	nodes=ID,...	Nodes to set at once (max 20)\n"
	"	from=ORIGINATOR	(local|*user|rain|timer|security|ups|sfc|lsc|program|...\n"
	"			wind|self|cycle|emergency)\n"
	"	prio=PRIO	(humanprot|envprot|user1|*user2|comfort(1..4))\n"
	"	nodes=ID,...	Nodes to set at once (max 20)\n"
	"	session=IDX	Overrule automatic session number\n"
	"	time=SEC	Wink for SEC seconds\n"
	"\n"
	"Options\n"
	" -V, --version		Show version\n"
	" -v, --verbose		Be more verbose\n"
	" -h, --host=URI	Specify KLF200 hostname(:port)\n"
	" -t, --time		Show times in log\n"
	" -w, --wait		Wait for events at end of program\n"
	;

#ifdef _GNU_SOURCE
static struct option long_opts[] = {
	{ "help", no_argument, NULL, '?', },
	{ "version", no_argument, NULL, 'V', },
	{ "verbose", no_argument, NULL, 'v', },

	{ "host", required_argument, NULL, 'h', },
	{ "time", no_argument, NULL, 't', },
	{ "wait", no_argument, NULL, 'w', },

	{ },
};
#else
#define getopt_long(argc, argv, optstring, longopts, longindex) \
	getopt((argc), (argv), (optstring))
#endif
static const char optstring[] = "Vv?h:tw";

static char *uri;
static int wait;

static char *addr_table_file;

/* signal handler */
static volatile int sigterm;
static volatile int sigalrm;

static void sighandler(int signr)
{
	switch (signr) {
	case SIGINT:
	case SIGTERM:
		sigterm = 1;
		break;
	case SIGALRM:
		sigalrm = 1;
		break;
	}
}

/* some more string tables */
static const char *const klf200_contr_copy_get[256] = {
	[0] = "ok",
	[2] = "failed, no remote controller in give mode detected",
	[4] = "failed, DTS not ready",
	[5] = "failed, DTS error, must return to virgin state",
	[9] = "failed, CS not ready",
};
static const char *const klf200_contr_copy_give[256] = {
	[0] = "ok",
	[1] = "failed, transfer interrupted",
	[4] = "ok, remote cancelled",
	[5] = "failed, timeout",
	[11] = "failed, CS not ready",
};
static const char *const klf200_newkey_str[256] = {
	[0] = "ok, key changed",
	[2] = "ok, key changed, all nodes updated",
	[3] = "ok, key changed, not all nodes updated",
	[5] = "ok, received a key",
	[7] = "failed, see bits",
	[9] = "failed, no controller found to get key from",
	[10] = "failed, DTS not ready",
	[11] = "failed, DTS error, must return to virgin state",
	[16] = "failed, CS not ready",
};
static const char *const klf200_pgc_state[256] = {
	[0] = "PGC job started",
	[1] = "PGC job ended",
	[2] = "CS busy",
};
static const char *const klf200_pgc_status[256] = {
	[0] = "ok",
	[1] = "partial success",
	[2] = "error in PGC/CS job",
	[3] = "bad keypress, or CS cancelled",
};
static const char *const klf200_pgc_type[256] = {
	[0] = "recv key or system",
	[1] = "recv key and distribute",
	[2] = "transmit key or system",
	[3] = "generate key and distribute",
};
static const char *const klf200_node_info_str[256] = {
	[0] = "ok",
	[1] = "rejected",
	[2] = "invalid parameter",
};
static const char *const klf200_activate_group_str[256] = {
	[0] = "ok",
	[1] = "unkonwn product group id",
	[2] = "SessionID already in use",
	[3] = "Busy, try again later",
	[4] = "Wrong group type",
	[6] = "Invalid parameter",
};

static int getbit(const void *vdat, int bitnr)
{
	const uint8_t *dat = vdat;

	return (dat[bitnr / 8] >> (bitnr % 8)) & 1;
}

static const char *masktostr(const void *vdat)
{
	static char buf[2048];
	char *str;
	const uint8_t *dat = vdat;
	int node;

	str = buf;

	for (node = 0; node < 204; ++node)
		if (getbit(dat, node))
			str += sprintf(str, "%s%u", (str != buf) ? " " : "", node);
	return buf;
}

static int print_group(const void *vdat, int len)
{
	const uint8_t *dat = vdat;

	if (len != 99)
		return -1;

	mylog(LOG_NOTICE, "group %i '%.64s', type %s, order %u, placement %u, v %s, rev %u",
			dat[0], dat+4,
			klf200_string(klf200_grouptype_str, dat[70]),
			klf200_get16(dat+1), dat[2],
			klf200_string(klf200_velocity_str, dat[68]),
			klf200_get16(dat+97));
	mylog(LOG_NOTICE, "nodes %s", masktostr(dat+72));
	return 0;
}

static void myklf200printpacket(void)
{
	const uint8_t *dat = klf200_pktdat();
	int len = klf200_pktlen();
	int cmd = klf200_pktcmd();
	int idx, n;

	switch (cmd) {
	case GW_ERROR_NTF:
		if (len != 1)
			goto invalid;
		mylog(LOG_ERR, "recvd error: %s", klf200_string(klf200_error_str, dat[0]));
		break;
	case GW_PASSWORD_ENTER_CFM:
	case GW_PASSWORD_CHANGE_CFM:
	case GW_SET_FACTORY_DEFAULT_CFM:
	case GW_LEAVE_LEARN_STATE_CFM:
		/* generic boolean response */
		if (len != 1)
			goto invalid;
		mylog(LOG_NOTICE, "%s", dat[0] ? "nok" : "ok");
		if (dat[0])
			exit(1);
		break;
	case GW_PASSWORD_CHANGED_NTF:
		if (len != 32)
			goto invalid;
		mylog(LOG_NOTICE, "password changed to '%.32s'", (char *)dat);
		break;
	case GW_GET_VERSION_CFM:
		if (len != 9)
			goto invalid;
		mylog(LOG_NOTICE, "version %u %u.%u.%u b%u.%u, hw %u, product %u.%u"
				, dat[0], dat[1], dat[2], dat[3]
				, dat[4], dat[5], dat[6]
				, dat[7], dat[8]
		     );
		break;
	case GW_GET_PROTOCOL_VERSION_CFM:
		if (len != 4)
			goto invalid;
		mylog(LOG_NOTICE, "protocol %u.%u", klf200_get16(dat),
				klf200_get16(dat+2));
		break;
	case GW_GET_STATE_CFM:
		if (len != 6)
			goto invalid;
		mylog(LOG_NOTICE, "state %s: %s",
				klf200_string(klf200_state_str, dat[0]),
				klf200_string(klf200_substate_str, dat[1]));
		break;
	case GW_GET_NETWORK_CFM:
		if (len != 13)
			goto invalid;
		mylog(LOG_NOTICE, "network %u.%u.%u.%u/%u.%u.%u.%u, gw %u.%u.%u.%u, dhcp: %u",
				dat[0], dat[1], dat[2], dat[3],
				dat[4], dat[5], dat[6], dat[7],
				dat[8], dat[9], dat[10], dat[11],
				dat[12]);
		break;
	case GW_GET_LOCAL_TIME_CFM:
		if (len < 4)
			goto invalid;
		mylog(LOG_NOTICE, "time is %s", klf200_timestr(klf200_get32(dat)));
		break;
	case GW_REBOOT_CFM:
	case GW_SET_UTC_CFM:
	case GW_SET_NETWORK_CFM:
	case GW_CS_GET_SYSTEMTABLE_DATA_CFM:
	case GW_CS_DISCOVER_NODES_CFM:
	case GW_CS_VIRGIN_STATE_CFM:
	case GW_CS_CONTROLLER_COPY_CFM:
	case GW_CS_GENERATE_NEW_KEY_CFM:
	case GW_CS_RECEIVE_KEY_CFM:
	case GW_CS_REPAIR_KEY_CFM:
	case GW_HOUSE_STATUS_MONITOR_ENABLE_CFM:
	case GW_HOUSE_STATUS_MONITOR_DISABLE_CFM:
	case GW_CLEAR_ACTIVATION_LOG_CFM:
		if (len != 0)
			goto invalid;
		mylog(LOG_NOTICE, "ok");
		break;
	case GW_CS_REMOVE_NODES_CFM:
		if (len != 1)
			goto invalid;
		mylog(LOG_NOTICE, "%s", dat[0] ? "ok" : "none");
		break;
	case GW_CS_GET_SYSTEMTABLE_DATA_NTF:
		if (len < 1 || (dat[0]*11+2 != len))
			goto invalid;
		n = dat[0];
		for (idx = 0, ++dat; idx < n; ++idx, dat += 11)
			mylog(LOG_NOTICE, "table %i, addr %06x, type %u.%u (%s), manf %s, ref %06x, lp %u, rf %u, round-trip %u",
					dat[0], klf200_get24(dat+1),
					klf200_get16(dat+4) >> 6, dat[5] & 0x3f,
					klf200_string(klf200_node_type_str, klf200_get16(dat+4) >> 6),
					klf200_string(klf200_manufacturer_str, dat[7]),
					klf200_get24(dat+8),
					(dat[6] >> 0) & 0x3,
					(dat[6] >> 3) & 0x1,
					(dat[6] >> 6) & 0x3);
		if (!*dat)
			mylog(LOG_NOTICE, "end-of-table");
		break;
	case GW_CS_DISCOVER_NODES_NTF:
		if (len != 131)
			goto invalid;
		if (dat[130] == 5)
			mylog(LOG_ERR, "box not ready for configuration service");
		else if (dat[130] == 7)
			mylog(LOG_ERR, "configuration service busy");
		else if (dat[130] == 6)
			mylog(LOG_WARNING, "discover succeeded with problems");
		klf200_log_node_masks(LOG_WARNING, dat, len,
				(const char *[]){ "added", "rf-error", "key-error", "removed", "open", }, 5);
		break;
	case GW_CS_CONTROLLER_COPY_NTF:
		if (len != 2)
			goto invalid;
		mylog(LOG_WARNING, "%s controller copy: %s",
				dat[0] ? "give" : "get",
				klf200_string(dat[0] ? klf200_contr_copy_give : klf200_contr_copy_get, dat[1]));
		break;
	case GW_CS_GENERATE_NEW_KEY_NTF:
	case GW_CS_RECEIVE_KEY_NTF:
	case GW_CS_REPAIR_KEY_NTF:
		if (len != 53)
			goto invalid;
		mylog(LOG_NOTICE, "%s", klf200_string(klf200_newkey_str, dat[0]));
		klf200_log_node_masks(LOG_WARNING, dat+1, len-1,
				(const char *[]){ "changed", "unchanged", }, 2);
		break;
	case GW_CS_SYSTEM_TABLE_UPDATE_NTF:
		if (len != 52)
			goto invalid;
		klf200_log_node_masks(LOG_WARNING, dat, len,
				(const char *[]){ "added", "removed", }, 2);
		break;
	case GW_CS_ACTIVATE_CONFIGURATION_MODE_CFM:
		if (len != 79)
			goto invalid;
		klf200_log_node_masks(LOG_WARNING, dat, len,
				(const char *[]){ "activated", "no-contact", "error", }, 3);
		mylog(LOG_NOTICE, "%s %u", dat[78] ? "nok" : "ok", dat[78]);
		break;
	case GW_CS_PGC_JOB_NTF:
		if (len != 3)
			goto invalid;
		mylog(LOG_NOTICE, "%s, %s, %s",
				klf200_string(klf200_pgc_state, dat[0]),
				klf200_string(klf200_pgc_status, dat[1]),
				klf200_string(klf200_pgc_type, dat[2]));
		break;
	case GW_GET_NODE_INFORMATION_CFM:
	case GW_SET_NODE_NAME_CFM:
	case GW_SET_NODE_VELOCITY_CFM:
	case GW_GET_GROUP_INFORMATION_CFM:
	case GW_SET_GROUP_INFORMATION_CFM:
	case GW_NEW_GROUP_CFM:
		if (len != 2)
			goto invalid;
		mylog(LOG_NOTICE, "idx %u: %s", dat[1], klf200_string(klf200_node_info_str, dat[0]));
		break;
	case GW_DELETE_GROUP_CFM:
		if (len != 2)
			goto invalid;
		mylog(LOG_NOTICE, "idx %u: %s", dat[0], klf200_string(klf200_node_info_str, dat[1]));
		break;
	case GW_NODE_INFORMATION_CHANGED_NTF:
		if (len != 69)
			goto invalid;
		mylog(LOG_NOTICE, "node %i changed: '%.64s', order %u, placement %u, var %u",
				dat[0], dat+1, klf200_get16(dat+65), dat[67], dat[68]);
		break;
	case GW_NODE_STATE_POSITION_CHANGED_NTF:
		if (len != 20)
			goto invalid;
		mylog(LOG_NOTICE, "node %i changed: state %s, pos %s / %s, %s, %s, %s, %s",
				dat[0],
				klf200_string(klf200_nodestate_str, dat[1]),
				klf200_decode_str(klf200_get16(dat+2)),
				klf200_decode_str(klf200_get16(dat+4)),
				klf200_decode_str(klf200_get16(dat+6)),
				klf200_decode_str(klf200_get16(dat+8)),
				klf200_decode_str(klf200_get16(dat+10)),
				klf200_decode_str(klf200_get16(dat+12)));
		break;
	case GW_GET_NODE_INFORMATION_NTF:
	case GW_GET_ALL_NODES_INFORMATION_NTF:
		if (len != 124)
			goto invalid;
		mylog(LOG_NOTICE, "node %i '%.64s', order %u, placement %u, vel %s, type %u.%u (%s)",
				dat[0], dat+4, klf200_get16(dat+1), dat[3],
				klf200_string(klf200_velocity_str, dat[68]),
				klf200_get16(dat+69) >> 6, dat[70] & 0x3f,
				klf200_string(klf200_node_type_str, klf200_get16(dat+69) >> 6));
		mylog(LOG_NOTICE, "pg %u, pt %u, var %u, pwr %u, build %u, serial %016llx",
				dat[71], dat[72], dat[73], dat[74], dat[75], (unsigned long long)klf200_get64(dat+76));
		mylog(LOG_NOTICE, "state %s, pos %s / %s, %s, %s, %s, %s",
				klf200_string(klf200_nodestate_str, dat[84]),
				klf200_decode_str(klf200_get16(dat+85)),
				klf200_decode_str(klf200_get16(dat+87)),
				klf200_decode_str(klf200_get16(dat+89)),
				klf200_decode_str(klf200_get16(dat+91)),
				klf200_decode_str(klf200_get16(dat+93)),
				klf200_decode_str(klf200_get16(dat+95)));
		mylog(LOG_NOTICE, "time %u+%u",
				klf200_get32(dat+99),
				klf200_get16(dat+97));
		break;
	case GW_GET_GROUP_INFORMATION_NTF:
	case GW_GET_ALL_GROUPS_INFORMATION_NTF:
		if (print_group(dat, len) < 0)
			goto invalid;
		break;
	case GW_GROUP_INFORMATION_CHANGED_NTF:
		if (len < 1)
			goto invalid;
		switch (dat[0]) {
		case 0: /* group deleted */
			if (len < 2)
				goto invalid;
			mylog(LOG_NOTICE, "group %i removed", dat[1]);
			break;
		case 1: /* group modified */
			if (len < 100)
				goto invalid;
			mylog(LOG_NOTICE, "group %i modified", dat[1]);
			if (print_group(dat+1, len-1) < 0)
				goto invalid;
			break;
		default:
			mylog(LOG_WARNING, "unknown group information changed notification %u", dat[0]);
			goto invalid;
			break;
		}
		break;
	case GW_GET_ALL_NODES_INFORMATION_FINISHED_NTF:
	case GW_GET_ALL_GROUPS_INFORMATION_FINISHED_NTF:
		mylog(LOG_NOTICE, "all received");
		break;
	case GW_GET_ALL_NODES_INFORMATION_CFM:
	case GW_GET_ALL_GROUPS_INFORMATION_CFM:
		if (len != 2)
			goto invalid;
		mylog(LOG_NOTICE, "%u, %u elements", dat[0], dat[1]);
		break;
	case GW_ACTIVATION_LOG_UPDATED_NTF:
		mylog(LOG_NOTICE, "activation logged");
		break;
	case GW_GET_ACTIVATION_LOG_HEADER_CFM:
		if (len != 4)
			goto invalid;
		mylog(LOG_NOTICE, "log: %u/%u", klf200_get16(dat+2), klf200_get16(dat+0));
		break;
	case GW_GET_MULTIPLE_ACTIVATION_LOG_LINES_CFM:
		if (len != 3)
			goto invalid;
		mylog(LOG_NOTICE, "log %s, %u lines", dat[2] ? "ok" : "fail", klf200_get16(dat));
		break;
	case GW_GET_MULTIPLE_ACTIVATION_LOG_LINES_NTF:
	case GW_GET_ACTIVATION_LOG_LINE_CFM:
		if (len != 17)
			goto invalid;

		printf("%s session %u: %s set node %u.%u to %s: %s, %s, info %08x\n",
				klf200_timestr(klf200_get32(dat)),
				klf200_get16(dat+4),
				klf200_string(klf200_owner_str, dat[6]),
				dat[7], dat[8], klf200_decode_str(klf200_get16(dat+9)),
				klf200_string(klf200_runstatus_str, dat[11]),
				klf200_string(klf200_statusreply_str, dat[12]),
				klf200_get32(dat+13));
		fflush(stdout);
		break;
	case GW_COMMAND_SEND_CFM:
	case GW_STATUS_REQUEST_CFM:
		if (len != 3)
			goto invalid;
		mylog(LOG_NOTICE, "session %u: %s", klf200_get16(dat), dat[2] ? "accepted" : "rejected");
		break;
	case GW_ACTIVATE_PRODUCTGROUP_CFM:
		if (len != 3)
			goto invalid;
		mylog(LOG_NOTICE, "session %u: %s", klf200_get16(dat), klf200_string(klf200_activate_group_str, dat[2]));
		break;
	case GW_WINK_SEND_NTF:
		if (len != 2)
			goto invalid;
		mylog(LOG_NOTICE, "session %u", klf200_get16(dat));
		break;
	case GW_COMMAND_RUN_STATUS_NTF:
		if (len != 13)
			goto invalid;
		mylog(LOG_NOTICE, "session %u: %s set node %u.%u to %s: %s, %s, info %08x",
				klf200_get16(dat),
				klf200_string(klf200_owner_str, dat[2]),
				dat[3], dat[4],
				klf200_decode_str(klf200_get16(dat+5)),
				klf200_string(klf200_runstatus_str, dat[7]),
				klf200_string(klf200_statusreply_str, dat[8]),
				klf200_get32(dat+9));
		break;
	case GW_COMMAND_REMAINING_TIME_NTF:
		if (len != 6)
			goto invalid;
		mylog(LOG_NOTICE, "session %u: %u.%u: %usec",
				klf200_get16(dat), dat[2], dat[3],
				klf200_get16(dat+4));
		break;
	case GW_SESSION_FINISHED_NTF:
		if (len != 2)
			goto invalid;
		mylog(LOG_NOTICE, "session %u done", klf200_get16(dat));
		break;
	case GW_STATUS_REQUEST_NTF:
		if (len < 7)
			goto invalid;
		if (dat[6] < 3) {
			if (len != 59)
				goto invalid;
			mylog(LOG_NOTICE, "session %u: %u.%u: %s %s",
					klf200_get16(dat), dat[2], dat[3],
					klf200_string(klf200_runstatus_str, dat[4]),
					klf200_string(klf200_statusreply_str, dat[5]));
		} else if (dat[6] == 3) {
			if (len != 18)
				goto invalid;
			char addrstr[16];
			sprintf(addrstr, "%06x", klf200_get24(dat+13));
			mylog(LOG_NOTICE, "session %u: %u.%u -> %s/%s from %s (%s), %s, +%us: %s %s",
					klf200_get16(dat), dat[2], dat[3],
					klf200_decode_str(klf200_get16(dat+7)),
					klf200_decode_str(klf200_get16(dat+9)),
					addrstr, libtable_lookup(addrstr, addr_table_file, LTL_FNMATCH) ?: "",
					klf200_string(klf200_owner_str, dat[17]),
					klf200_get16(dat+11),
					klf200_string(klf200_runstatus_str, dat[4]),
					klf200_string(klf200_statusreply_str, dat[5]));
		}
		break;
	default:
invalid:
		mylog(LOG_WARNING, "recvd pkt %04x+%u", cmd, len);
		break;
	}
}

static int myklf200recv(int sock, int rcmd, int timeout)
{
	int ret, cmd;

	alarm(0);
	sigalrm = 0;
	/* set signal handler each time */
	signal(SIGALRM, sighandler);
	alarm(timeout);

	for (; !sigalrm && !sigterm;) {
		struct pollfd pfd = { .fd = sock, .events = POLLIN, };
		if (poll(&pfd, 1, 1000) <= 0)
			continue;

		ret = klf200_recv(sock);
		if (ret < 0)
			goto done;
		for (;;) {
			ret = cmd = klf200_next_packet(sock);
			if (cmd < 0)
				continue;
			/* verify if complete packet has been received */
			if (cmd == GW_N_COMMAND)
				/* read more data ... */
				break;
			myklf200printpacket();
			if (cmd == rcmd)
				goto done;
		}
	}
	if (sigalrm && rcmd != GW_N_COMMAND)
		mylog(LOG_NOTICE, "timeout");
	ret = -1;
done:
	alarm(0);
	return ret;
}

static void myklf200xfer2(int sock, int cmd, int rcmd, const void *tdat, int tlen, int timeout)
{
	int ret;

	ret = klf200_send(sock, 0, cmd, tdat, tlen);
	if (ret >= 0)
		ret = myklf200recv(sock, rcmd, timeout);
	if (ret < 0)
		mylog(LOG_ERR, "failed");
}
static inline void myklf200xfer(int sock, int cmd, const void *tdat, int tlen, int timeout)
{
	myklf200xfer2(sock, cmd, cmd+1, tdat, tlen, timeout);
}

#define testnargs(n, cmd) \
	({ \
		if ((optind + (n)) >= argc) \
			mylog(LOG_ERR, "command %s requires at least %u arguments", (cmd), (n)); \
	 })

static uint8_t tdat[256];
int main(int argc, char *argv[])
{
	int opt, result = 1;

	/* argument parsing */
	while ((opt = getopt_long(argc, argv, optstring, long_opts, NULL)) >= 0)
	switch (opt) {
	case 'V':
		fprintf(stderr, "%s %s\nCompiled on %s %s\n",
				NAME, VERSION, __DATE__, __TIME__);
		exit(0);
	case '?':
		fputs(help_msg, stderr);
		exit(0);
	default:
		fprintf(stderr, "unknown option '%c'", opt);
		fputs(help_msg, stderr);
		exit(1);
		break;
	case 'v':
		++max_loglevel;
		break;
	case 'h':
		uri = optarg;
		break;
	case 't':
		logtime = 1;
		break;
	case 'w':
		wait = 1;
		break;
	}

	if (!uri)
		mylog(LOG_ERR, "no host given, run with -h HOST");

	char *veluxpwd = NULL;
	if (lib_netrc(uri, NULL, &veluxpwd) < 0 || !veluxpwd)
		mylog(LOG_ERR, "no password found for %s in ~/.netrc", uri);

	asprintf(&addr_table_file, "%s/iohc-addr-table", getenv("HOME") ?: "/var/lib");

	/* connect */
	mylog(LOG_INFO, "connect to %s ...", uri);
	__attribute__((unused))
	int sock = klf200_connect(uri);

	signal(SIGTERM, sighandler);
	signal(SIGINT, sighandler);

	mylog(LOG_INFO, "authenticate");
	memset(tdat+0, 0, 32);
	strncpy((char *)tdat, veluxpwd, 32);
	myklf200xfer(sock, GW_PASSWORD_ENTER_REQ, tdat, 32, 1);
	if (sigterm)
		goto done;
	memset(tdat, 0, sizeof(tdat));
	memset(veluxpwd, 0, strlen(veluxpwd));
	free(veluxpwd);

	mylog(LOG_INFO, "get version");
	myklf200xfer(sock, GW_GET_VERSION_REQ, NULL, 0, 1);
	if (sigterm)
		goto done;

	mylog(LOG_INFO, "get protocol");
	myklf200xfer(sock, GW_GET_PROTOCOL_VERSION_REQ, NULL, 0, 1);
	if (sigterm)
		goto done;

	mylog(LOG_INFO, "get state");
	myklf200xfer(sock, GW_GET_STATE_REQ, NULL, 0, 1);
	if (sigterm)
		goto done;

	/* process arguments */
	char *cmd;
	for (; !sigterm && optind < argc; ++optind) {
		cmd = argv[optind];

		if (!strcmp(cmd, "reboot")) {
			mylog(LOG_INFO, "reboot");
			myklf200xfer(sock, GW_REBOOT_REQ, NULL, 0, 1);

		} else if (!strcmp(cmd, "factory")) {
			mylog(LOG_INFO, "factory");
			myklf200xfer(sock, GW_SET_FACTORY_DEFAULT_REQ, NULL, 0, 1);

		} else if (!strcmp(cmd, "stoplearn")) {
			mylog(LOG_INFO, "stop learning");
			myklf200xfer(sock, GW_LEAVE_LEARN_STATE_REQ, NULL, 0, 1);

		} else if (!strcmp(cmd, "setpwd")) {
			testnargs(1, cmd);
			mylog(LOG_INFO, "set password");
			memset(tdat+0, 0, 64);
			strcpy((char *)tdat, "XXX");
			strcpy((char *)tdat+32, argv[optind++]);
			myklf200xfer(sock, GW_PASSWORD_CHANGE_REQ, tdat, 64, 1);

		} else if (!strcmp(cmd, "network")) {
			testnargs(1, cmd);
			++optind;
			if (!strcmp(argv[optind], "?")) {
				mylog(LOG_INFO, "get network");
				myklf200xfer(sock, GW_GET_NETWORK_REQ, NULL, 0, 1);
			} else if (!strcmp(argv[optind], "dhcp")) {
				memset(tdat, 0, 12);
				tdat[12] = 1;
				myklf200xfer(sock, GW_SET_NETWORK_REQ, tdat, 12, 1);
			}
		} else if (!strcmp(cmd, "time")) {
			testnargs(1, cmd);
			cmd = argv[++optind];

			if (!strcmp(cmd, "sync")) {
				mylog(LOG_INFO, "time %s", cmd);
				klf200_put32(tdat, time(NULL));
				myklf200xfer(sock, GW_SET_UTC_REQ, tdat, 4, 1);

			} else {
				mylog(LOG_INFO, "time get");
				myklf200xfer(sock, GW_GET_LOCAL_TIME_REQ, NULL, 0, 1);
			}

		} else if (!strcmp(cmd, "table")) {
			mylog(LOG_INFO, "get system table");
			myklf200xfer(sock, GW_CS_GET_SYSTEMTABLE_DATA_REQ, NULL, 0, 1);
			myklf200recv(sock, GW_CS_GET_SYSTEMTABLE_DATA_NTF, 1);
			for (; klf200_entries_to_come();)
				if (myklf200recv(sock, GW_CS_GET_SYSTEMTABLE_DATA_NTF, 1) < 0)
					break;

		} else if (!strcmp(cmd, "discover")) {
			mylog(LOG_INFO, "discovering");
			tdat[0] = 0; /* node type to discover: all */
			myklf200xfer(sock, GW_CS_DISCOVER_NODES_REQ, tdat, 1, 1);
			myklf200recv(sock, GW_CS_DISCOVER_NODES_NTF, 60);

		} else if (!strcmp(cmd, "remove")) {
			testnargs(1, cmd);
			opt = klf200_nodestrtobitmask(argv[++optind], tdat, 26);
			mylog(LOG_INFO, "remove %u nodes", opt);
			myklf200xfer(sock, GW_CS_REMOVE_NODES_REQ, tdat, 26, 1);

		} else if (!strcmp(cmd, "config")) {
			testnargs(1, cmd);
			opt = klf200_nodestrtobitmask(argv[++optind], tdat, 26);
			mylog(LOG_INFO, "open %u nodes", opt);
			myklf200xfer(sock, GW_CS_ACTIVATE_CONFIGURATION_MODE_REQ, tdat, 26, 1);

		} else if (!strcmp(cmd, "virgin")) {
			mylog(LOG_INFO, "set virgin state");
			myklf200xfer(sock, GW_CS_VIRGIN_STATE_REQ, NULL, 0, 1);

		} else if (!strcmp(cmd, "copy")) {
			testnargs(1, cmd);
			++optind;

			if (!strcmp(argv[optind], "get"))
				tdat[0] = 0;
			else if (!strcmp(argv[optind], "give"))
				tdat[0] = 1;
			else if (!strcmp(argv[optind], "cancel")) {
				/* special transmit: not response expected */
				if (klf200_send(sock, 0, GW_CS_CONTROLLER_COPY_CANCEL_NTF, NULL, 0) < 0)
					mylog(LOG_ERR, "failed");
				continue;
			} else
				mylog(LOG_ERR, "copy '%s' unknown", argv[optind]);

			mylog(LOG_INFO, "%s controller copy", argv[optind]);
			myklf200xfer(sock, GW_CS_CONTROLLER_COPY_REQ, tdat, 1, 1);

		} else if (!strcmp(cmd, "key")) {
			testnargs(1, cmd);
			int cmd;
			++optind;

			if (!strcmp(argv[optind], "new"))
				cmd = GW_CS_GENERATE_NEW_KEY_REQ;
			else if (!strcmp(argv[optind], "recv"))
				cmd = GW_CS_RECEIVE_KEY_REQ;
			else if (!strcmp(argv[optind], "repair"))
				cmd = GW_CS_REPAIR_KEY_REQ;
			else {
				mylog(LOG_WARNING, "key '%s' unknown", argv[optind]);
				continue;
			}

			mylog(LOG_INFO, "%s key", argv[optind]);
			myklf200xfer(sock, cmd, NULL, 0, 1);

		} else if (!strcmp(cmd, "monitor")) {
			testnargs(1, cmd);
			int cmd;
			++optind;

			if (!strcmp(argv[optind], "on"))
				cmd = GW_HOUSE_STATUS_MONITOR_ENABLE_REQ;
			else if (!strcmp(argv[optind], "off"))
				cmd = GW_HOUSE_STATUS_MONITOR_DISABLE_REQ;
			else {
				mylog(LOG_WARNING, "monitor '%s' unknown", argv[optind]);
				continue;
			}

			mylog(LOG_INFO, "monitor %s", argv[optind]);
			myklf200xfer(sock, cmd, NULL, 0, 1);

		} else if (!strcmp(cmd, "node")) {
			testnargs(1, cmd);
			++optind;

			if (!strcmp(argv[optind], "all")) {
				mylog(LOG_INFO, "node %s", argv[optind]);
				myklf200xfer(sock, GW_GET_ALL_NODES_INFORMATION_REQ, NULL, 0, 1);
				myklf200recv(sock, GW_GET_ALL_NODES_INFORMATION_FINISHED_NTF, 10);

			} else if (!strcmp(argv[optind], "name")) {
				testnargs(2, "node name");
				mylog(LOG_INFO, "node %s", argv[optind+1]);
				tdat[0] = strtoul(argv[optind+1], NULL, 0);
				memset(tdat+1, 0, 64);
				strncpy((char *)tdat+1, argv[optind+2], 64);
				myklf200xfer(sock, GW_SET_NODE_NAME_REQ, tdat, 65, 1);
				optind += 2;

			} else if (!strcmp(argv[optind], "velocity")) {
				testnargs(2, "node velocity");
				mylog(LOG_INFO, "node %s", argv[optind+1]);
				tdat[0] = strtoul(argv[optind+1], NULL, 0);
				tdat[1] = klf200_string_idx(argv[optind+2], klf200_velocity_str, 256);
				myklf200xfer(sock, GW_SET_NODE_VELOCITY_REQ, tdat, 2, 1);
				optind += 2;

			} else {
				tdat[0] = strtoul(argv[optind], NULL, 0);
				mylog(LOG_INFO, "node %s", argv[optind]);
				myklf200xfer(sock, GW_GET_NODE_INFORMATION_REQ, tdat, 1, 1);
				myklf200recv(sock, GW_GET_NODE_INFORMATION_NTF, 1);
			}

		} else if (!strcmp(cmd, "group")) {
			testnargs(1, cmd);
			++optind;

			if (optind >= argc || !strcmp(argv[optind], "all")) {
				mylog(LOG_INFO, "group %s", argv[optind]);
				memset(tdat, 0, 2);
				myklf200xfer(sock, GW_GET_ALL_GROUPS_INFORMATION_REQ, tdat, 2, 1);
				myklf200recv(sock, GW_GET_ALL_GROUPS_INFORMATION_FINISHED_NTF, 10);

			} else if (!strcmp(argv[optind], "add") || !strcmp(argv[optind], "modify")) {
				int cmdchr = argv[optind][0];
				++optind;

				int offs;

				/* set & add have similar packet layout, but set prepend the group id */
				if (cmdchr != 'a') {
					/* read ... */
					mylog(LOG_NOTICE, "read group ...");
					tdat[0] = strtoul(argv[optind++], NULL, 0);
					myklf200xfer(sock, GW_GET_GROUP_INFORMATION_REQ, tdat, 1, 1);
					myklf200recv(sock, GW_GET_GROUP_INFORMATION_NTF, 1);
					memcpy(tdat, klf200_pktdat(), klf200_pktlen());
					offs = 1;
				} else {
					memset(tdat, 0, 96);
					offs = 0;
				}

				int mods;
				for (mods = 0; optind < argc; ++optind, ++mods) {
					char *key, *value;

					key = argv[optind];
					value = strchr(key, '=');
					if (!value) {
						--optind;
						break;
					}
					/* insert 0 seperator */
					*value++ = 0;
					if (!strcmp(key, "name")) {
						strncpy((char *)tdat+offs+3, value, 64);
					} else if (!strcmp(key, "type")) {
						tdat[offs+69] = klf200_string_idx(value, klf200_grouptype_str, 4);

					} else if (!strcmp(key, "nodes")) {
						memset(tdat+offs+71, 0, 26);
						tdat[offs+70] = klf200_nodestrtobitmask(value, tdat+offs+71, 26);

					} else if (!strcmp(key, "velocity")) {
						tdat[offs+67] = klf200_string_idx(value, klf200_velocity_str, 256);

					} else {
						mylog(LOG_ERR, "unknown key '%s'", key);
					}
				}
				if (cmdchr != 'a' && !mods)
					mylog(LOG_ERR, "not changing group information since no changes requested");
				mylog(LOG_NOTICE, "%s group", (cmdchr == 'a') ? "add" : "modify");
				myklf200xfer(sock, (cmdchr == 'a') ? GW_NEW_GROUP_REQ : GW_SET_GROUP_INFORMATION_REQ,
						tdat, offs+96, 1);

			} else if (!strcmp(argv[optind], "rm")) {
				if (optind +1 <= argc)
					mylog(LOG_ERR, "require 1 parameters for 'group nodes'");
				mylog(LOG_INFO, "remove group %s", argv[optind+1]);
				tdat[0] = strtoul(argv[optind+1], NULL, 0);
				myklf200xfer(sock, GW_DELETE_GROUP_REQ, tdat, 1, 1);
				++optind;

			} else if (!strcmp(argv[optind], "set")) {
				testnargs(1, "group set");

				memset(tdat, 0, sizeof(tdat));
				/* initialize defaults */
				klf200_put16(tdat, klf200_sessionid());
				tdat[2] = 1; /* owner/originator: user */
				tdat[3] = 3; /* priority level: user2 */
				tdat[4] = strtoul(argv[++optind], NULL, 0);
				klf200_put16(tdat+6, 0xd100); /* main value: not-a-number */

				/* scan additional parameters */
				char *key, *value, *endp;
				for (++optind; optind < argc; ++optind) {
					key = argv[optind];
					value = strchr(key, '=');
					if (!value) {
						--optind;
						break;
					}
					*value++ = 0;

					if (!strcmp(key, "value")) {
						tdat[5] = 0xff;
						if (!strncmp(value, "fp", 2))
							tdat[5] = strtoul(value+2, &endp, 10);
						if (tdat[5] > 16)
							mylog(LOG_ERR, "bad function parameter index '%s'", value);

					} else if (!strcmp(key, "from")) {
						tdat[2] = klf200_string_idx(value, klf200_owner_str, 256);
						if (tdat[2] == 0xff)
							mylog(LOG_ERR, "from '%s' unknown", value);

					} else if (!strcmp(key, "prio")) {
						tdat[3] = klf200_string_idx(value, klf200_prio_level_str, 8);
						if (tdat[3] == 0xff)
							mylog(LOG_ERR, "prio '%s' unknown", value);

					} else if (!strcmp(key, "velocity")) {
						tdat[8] = klf200_string_idx(value, klf200_velocity_str, 256);
						if (tdat[8] == 0xff)
							mylog(LOG_ERR, "velocity '%s' unknown", value);

					} else if (!strcmp(key, "session")) {
						klf200_put16(tdat, strtoul(value, NULL, 0));

					} else {
						mylog(LOG_ERR, "unknown key '%s'", key);
					}
				}
				/* perform action */
				myklf200xfer(sock, GW_ACTIVATE_PRODUCTGROUP_REQ, tdat, 13, 1);
				myklf200recv(sock, GW_SESSION_FINISHED_NTF, 60);


			} else {
				tdat[0] = strtoul(argv[optind], NULL, 0);
				mylog(LOG_INFO, "group %s", argv[optind]);
				myklf200xfer(sock, GW_GET_GROUP_INFORMATION_REQ, tdat, 1, 1);
				myklf200recv(sock, GW_GET_GROUP_INFORMATION_NTF, 1);
			}

		} else if (!strcmp(cmd, "log")) {
			testnargs(1, cmd);
			cmd = argv[++optind];

			if (!strcmp(cmd, "stat")) {
				mylog(LOG_INFO, "log %s", cmd);
				myklf200xfer(sock, GW_GET_ACTIVATION_LOG_HEADER_REQ, NULL, 0, 1);

			} else if (!strcmp(cmd, "clear")) {
				mylog(LOG_INFO, "log %s", cmd);
				myklf200xfer(sock, GW_CLEAR_ACTIVATION_LOG_REQ, NULL, 0, 1);

			} else if (!strcmp(cmd, "all")) {
				mylog(LOG_INFO, "log %s", cmd);
				memset(tdat, 0, 4);
				myklf200xfer2(sock, GW_GET_MULTIPLE_ACTIVATION_LOG_LINES_REQ,
						GW_GET_MULTIPLE_ACTIVATION_LOG_LINES_CFM, tdat, 4, 10);

			} else {
				mylog(LOG_ERR, "log '%s' unknown", argv[optind]);
			}
		} else if (!strcmp(cmd, "set")) {
			memset(tdat, 0, sizeof(tdat));
			/* initialize defaults */
			klf200_put16(tdat, klf200_sessionid());
			tdat[2] = 1; /* owner/originator: user */
			tdat[3] = 3; /* priority level: user2 */
			klf200_put16(tdat+7, 0xd400); /* main value: not-a-number */
			klf200_put16(tdat+63, 0xffff); /* fill lock with 'keep current' */

			/* scan additional parameters */
			char *key, *value, *endp;
			int valuemask = 0;
			int idx;

			for (++optind; optind < argc; ++optind) {

				key = argv[optind];
				value = strchr(key, '=');

				if (!value) {
					--optind;
					break;
				}
				*value++ = 0;

				if (!strcmp(key, "val")) {
					idx = 0;
					goto scan_param;
				} else if (!strncmp(key, "fp", 2)) {
					idx = strtoul(key+2, &endp, 10);
					if (endp <= key || idx > 16)
						mylog(LOG_ERR, "bad function parameter index '%s'", key+2);
scan_param:
					klf200_put16(tdat+7+2*idx, klf200_encode_str(value, NULL));
					valuemask |= 1 << idx;

				} else if (!strcmp(key, "nodes")) {
					tdat[41] = klf200_nodestrtolist(value, tdat+42, 20);

				} else if (!strcmp(key, "from")) {
					tdat[2] = klf200_string_idx(value, klf200_owner_str, 256);
					if (tdat[2] == 0xff)
						mylog(LOG_ERR, "from '%s' unknown", value);

				} else if (!strcmp(key, "prio")) {
					tdat[3] = klf200_string_idx(value, klf200_prio_level_str, 8);
					if (tdat[3] == 0xff)
						mylog(LOG_ERR, "prio '%s' unknown", value);

				} else if (!strcmp(key, "session")) {
					klf200_put16(tdat, strtoul(value, NULL, 0));

				} else if (!strcmp(key, "lock") || !strcmp(key, "unlock")) {
					uint16_t lockmask;
					int lockval = (*key == 'l') ? 0 : 1;

					lockmask = klf200_get16(tdat+63);
					idx = klf200_string_idx(value, klf200_prio_level_str, 8);
					if (idx == 0xff)
						mylog(LOG_ERR, "prio '%s' unknown", value);

					lockmask &= ~(3 << (14-idx*2));
					lockmask |= lockval << (14-idx*2);

					tdat[62] = 1; /* enable lock */
					klf200_put16(tdat+63, lockmask);

				} else if (!strcmp(key, "locktime")) {
					tdat[65] = strtoul(value, NULL, 0);

				} else {
					mylog(LOG_ERR, "unknown key '%s'", key);
				}
			}
			/* set valuemask */
			tdat[5] = valuemask >> 1;
			tdat[6] = valuemask >> 9;
			/* perform action */
			myklf200xfer(sock, GW_COMMAND_SEND_REQ, tdat, 66, 1);
			myklf200recv(sock, GW_SESSION_FINISHED_NTF, 60);

		} else if (!strcmp(cmd, "status")) {
			memset(tdat, 0, sizeof(tdat));
			/* initialize defaults */
			klf200_put16(tdat, klf200_sessionid());
			tdat[23] = 3;

			/* scan additional parameters */
			char *key, *value, *endp;
			int valuemask = 0;

			for (++optind; optind < argc; ++optind) {

				key = argv[optind];
				value = strchr(key, '=');
				if (!value) {
					--optind;
					break;
				}
				*value++ = 0;

				if (!strcmp(key, "nodes")) {
					tdat[2] = klf200_nodestrtolist(value, tdat+3, 20);

				} else if (!strcmp(key, "session")) {
					klf200_put16(tdat, strtoul(value, NULL, 0));

				} else if (!strcmp(key, "type")) {
					tdat[23] = klf200_string_idx(value, klf200_statustype_str, 256);
					if (tdat[23] == 0xff)
						mylog(LOG_ERR, "type '%s' unknown", value);

				} else if (!strcmp(key, "values")) {
					for (value = strtok(value, ","); value; value = strtok(NULL, ",")) {
						if (!strcmp(value, "val")) {
							valuemask |= 1;

						} else if (!strncmp(value, "fp", 2)) {
							int idx = strtoul(key+2, &endp, 10);
							if (endp <= key || idx > 16)
								mylog(LOG_ERR, "bad function parameter index '%s'", key+2);
							valuemask |= 1 << idx;
						} else {
							mylog(LOG_ERR, "unknown value '%s'", value);
						}
					}

				} else {
					mylog(LOG_ERR, "unknown key '%s'", key);
				}
			}
			if (!tdat[2]) {
				mylog(LOG_WARNING, "no nodes defined, skip '%s'", cmd);
				continue;
			}
			/* set valuemask */
			tdat[24] = valuemask >> 1;
			tdat[25] = valuemask >> 9;
			/* perform action */
			myklf200xfer(sock, GW_STATUS_REQUEST_REQ, tdat, 26, 1);
			myklf200recv(sock, GW_SESSION_FINISHED_NTF, 15);

		} else if (!strcmp(cmd, "wink")) {
			memset(tdat, 0, sizeof(tdat));
			/* initialize defaults */
			klf200_put16(tdat, klf200_sessionid());
			tdat[2] = 1; /* owner/originator: user */
			tdat[3] = 3; /* priority level: user2 */
			tdat[4] = 1; /* enable wink */
			tdat[5] = 5; /* wink time */

			/* scan additional parameters */
			char *key, *value;

			for (++optind; optind < argc; ++optind) {

				key = argv[optind];
				value = strchr(key, '=');
				if (!value) {
					--optind;
					break;
				}
				*value++ = 0;

				if (!strcmp(key, "session")) {
					klf200_put16(tdat, strtoul(value, NULL, 0));

				} else if (!strcmp(key, "from")) {
					tdat[2] = klf200_string_idx(value, klf200_owner_str, 256);
					if (tdat[2] == 0xff)
						mylog(LOG_ERR, "from '%s' unknown", value);

				} else if (!strcmp(key, "prio")) {
					tdat[3] = klf200_string_idx(value, klf200_prio_level_str, 8);
					if (tdat[3] == 0xff)
						mylog(LOG_ERR, "prio '%s' unknown", value);

				} else if (!strcmp(key, "time")) {
					tdat[5] = strtoul(value, NULL, 0);

				} else if (!strcmp(key, "nodes")) {
					tdat[6] = klf200_nodestrtolist(value, tdat+7, 20);

				} else {
					mylog(LOG_ERR, "unknown key '%s'", key);
				}
			}
			/* perform action */
			myklf200xfer(sock, GW_WINK_SEND_REQ, tdat, 27, 1);
			myklf200recv(sock, GW_WINK_SEND_NTF, 1);

		} else {
			mylog(LOG_ERR, "command '%s' unknown", cmd);
		}
	}

	for (; wait && !sigterm; ) {
		/* wait */
		myklf200recv(sock, GW_N_COMMAND /* never recv */, 120);
		if (sigterm)
			break;

		/* don't test result, we do not expect anything ... */
		mylog(LOG_INFO, "get state");
		myklf200xfer(sock, GW_GET_STATE_REQ, NULL, 0, 1);
	}

	result = !sigterm;
done:
	/* cleanup */
	klf200_cleanup();

	return result;
}
