#ifndef _libtable_h_
#define _libtable_h_

#ifdef __cplusplus
extern "C" {
#endif

#define LTL_REVERSE	0x01
#define LTL_FNMATCH	0x02

extern const char *libtable_lookup(const char *key, const char *filename, int flags);

#ifdef __cplusplus
}
#endif
#endif
